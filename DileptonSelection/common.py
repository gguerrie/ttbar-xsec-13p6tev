from ROOT import *
from array import array

# From Tomas lumi
# lumi_18 = 58450.1

# hence my luminosity used is
#iLumi   = 225.904 # Period D-E
iLumi   = 792.315 # Period F1
#iLumi   = 727.059 #temporary lumi for 1L/2L partial data

nb = 1e3
pb = 1.
fb = 1e-3

###### Cross-section values, need to cross-check each entry from XSection-MC21-13p6TeV.data
## only Top 410470 was modified

xs = {
   # SingleTop
   # s-channel
   '601348': 1.3525 * 1.0872*pb, #Powheg v2 + Pythia8
   '601349': 2.1455 * 1.0963*pb, #Powheg v2 + Pythia8
   # t-channel
   '601350': 24.203 * 1.0858*pb, #Powheg v2 + Pythia8
   '601351': 39.936 * 1.1066*pb, #Powheg v2 + Pythia8
   # tW inclusive
   '601352': 39.839 * 0.9924*pb, #Powheg v2 + Pythia8
   '601355': 39.876 * 0.9924*pb, #Powheg v2 + Pythia8
   # tW dilepton
   '601353': 4.1974 * 0.9924*pb, #Powheg v2 + Pythia8
   '601354': 4.2013 * 0.9924*pb, #Powheg v2 + Pythia8
   
   
   # Sherpa 2.2.10-12 diboson
   # "Semileptonic" VV sherpa 2.2.12 - not really semileptonic...
   '700566': 51.226 * 0.91 * pb, #sherpa2210
   '700567': 2.5961 * 0.91 * pb, #sherpa2210
   '700568': 9.2309 * 0.91 * pb, #sherpa2210
   '700569': 3.5549 * 0.91 * pb, #sherpa2210
   '700570': 7.0361 * 0.91 * pb, #sherpa2210
   '700571': 0.4986 * 0.91 * pb, #sherpa2210
   '700572': 1.7741 * 0.91 * pb, #sherpa2210
   '700573': 0.9876 * 0.91 * pb, #sherpa2210
   '700574': 3.5125 * 0.91 * pb, #sherpa2210
   # Multilepton+neutrino VV sherpa 2.2.12
   '700600': 1.3351 * 0.91 * pb, #sherpa2210
   '700601': 4.8181 * 0.91 * pb, #sherpa2210
   '700602': 12.618 * 0.91 * pb, #sherpa2210
   '700603': 0.0241 * 0.91 * pb, #sherpa2210
   '700604': 3.289 * 0.91 * pb, #sherpa2210
   '700605': 0.61379 * 0.91 * pb, #sherpa2210


   #TTBAR
   '601229': 355.63972 * 1.1381 * pb,
   '601230': 85.482 * 1.1381 * pb, # ttbar dilep
   '601237': 369.95387 * 1.1381 * pb, # Pythia8


   #ttbar modelling
   # Powheg + Herwig 7.1.6 ttbar - should be identical to herwig 7.1.3 for MC/MC
   '601414': 355.704 * 1.1387 * pb, #herwigpp713
   '601415': 85.54 * 1.1387 * pb, #herwigpp713
   # Powheg+Pythia8 ttbar hdamp = 3 times top mass
   '601398': 355.605 * 1.1390 * pb, #pythia8
   '601399': 85.46 * 1.1390 * pb, #pythia8


   
   #Zjets

   # Z(ee)+jets sherpa 2.2.12
   '700615': 59.841 * 0.9383 * pb, #sherpa2210
   '700616': 303.25 * 0.9383 * pb, #sherpa2210
   '700617': 1974.0 * 0.9383 * pb, #sherpa2210
   # Z(mumu)+jets sherpa 2.2.12
   '700618': 58.688 * 0.9383 * pb, #sherpa2210
   '700619': 304.48 * 0.9383 * pb, #sherpa2210
   '700620': 1974.2 * 0.9383 * pb, #sherpa2210
   # Z(tautau)+jets sherpa 2.2.12
   '700621': 59.655 * 0.9383 * pb, #sherpa2210
   '700622': 302.53 * 0.9383 * pb, #sherpa2210
   '700623': 1971.0 * 0.9383 * pb, #sherpa2210
   # Z(nunu)+jets sherpa 2.2.12
   '700624': 41.121 * 1.0 * pb, #sherpa2210
   '700625': 97.944 * 1.0 * pb, #sherpa2210
   '700626': 342.52 * 1.0 * pb, #sherpa2210
   # Z+jets 10 < m_ll < 40 sherpa 2.2.12
   '700575': 4266.4 * 1.0 * pb, #sherpa2210
   '700576': 4266.3 * 1.0 * pb, #sherpa2210
   '700577': 4265.6 * 1.0 * pb, #sherpa2210

   
   
   #Wjets
   # W(->e nu)+jets sherpa 2.2.12 
   '700606': 220.132 * 0.9059 * pb, #sherpa2210
   '700607': 3402.25 * 0.9059 * pb, #sherpa2210
   '700608': 19290.2 * 0.9059 * pb, #sherpa2210
   # W(->mu nu)+jets sherpa 2.2.12
   '700609': 218.007 * 0.9059 * pb, #sherpa2210
   '700610': 3411.47 * 0.9059 * pb, #sherpa2210
   '700611': 19326.7 * 0.9059 * pb, #sherpa2210
   # W(->tau nu)+jets sherpa 2.2.12
   '700612': 220.519 * 0.9059 * pb, #sherpa2210
   '700613': 3411.31 * 0.9059 * pb, #sherpa2210
   '700614': 19337.6 * 0.9059 * pb, #sherpa2210



  
  # Top Rel 22.
  # https://gitlab.cern.ch/tdado/ttbarxsecrun3/-/blob/master/data/XSection-MC21-13p6TeV.data says 410470 396.87 1.1398 pythia8
  #, so we multiply 396.87 x 1.1398 = 452.35243 - this is still 13 TeV
  # in principle it should be equal to the THEO> cross-section times BR
  # Branching ratio is: 1-(1-3*0.1082)^2=0.5438, 
  # THEO crosssection is (https://indico.cern.ch/event/1144393/contributions/4804148/attachments/2418904/4140014/Talk.pdf) 
  # at 13.6 TeV is 923 pb
  # at 13 TeV is 832 pb
  # multiplying those guys we have for 13 TeV we have 452.44160 which is almost 452.35243 (can ask Tomas why they are not exactly the same) 
}

#####################

GeV = 1e3
TeV = 1e6
m_W_PDG   = 80.4*GeV
m_top_PDG = 172.5*GeV

#####################

systematics_tree = [
   "nominal",
   "JET_BJES_Response__1down",
   "JET_BJES_Response__1up",
   "JET_EffectiveNP_Detector1__1down",
   "JET_EffectiveNP_Detector1__1up",
   "JET_EffectiveNP_Detector2__1down",
   "JET_EffectiveNP_Detector2__1up",
   "JET_EffectiveNP_Mixed1__1down",
   "JET_EffectiveNP_Mixed1__1up",
   "JET_EffectiveNP_Mixed2__1down",
   "JET_EffectiveNP_Mixed2__1up",
   "JET_EffectiveNP_Mixed3__1down",
   "JET_EffectiveNP_Mixed3__1up",
   "JET_EffectiveNP_Modelling1__1down",
   "JET_EffectiveNP_Modelling1__1up",
   "JET_EffectiveNP_Modelling2__1down",
   "JET_EffectiveNP_Modelling2__1up",
   "JET_EffectiveNP_Modelling3__1down",
   "JET_EffectiveNP_Modelling3__1up",
   "JET_EffectiveNP_Modelling4__1down",
   "JET_EffectiveNP_Modelling4__1up",
   "JET_EffectiveNP_Statistical1__1down",
   "JET_EffectiveNP_Statistical1__1up",
   "JET_EffectiveNP_Statistical2__1down",
   "JET_EffectiveNP_Statistical2__1up",
   "JET_EffectiveNP_Statistical3__1down",
   "JET_EffectiveNP_Statistical3__1up",
   "JET_EffectiveNP_Statistical4__1down",
   "JET_EffectiveNP_Statistical4__1up",
   "JET_EffectiveNP_Statistical5__1down",
   "JET_EffectiveNP_Statistical5__1up",
   "JET_EffectiveNP_Statistical6__1down",
   "JET_EffectiveNP_Statistical6__1up",
   "JET_EtaIntercalibration_Modelling__1down",
   "JET_EtaIntercalibration_Modelling__1up",
   "JET_EtaIntercalibration_NonClosure_2018data__1down",
   "JET_EtaIntercalibration_NonClosure_2018data__1up",
   "JET_EtaIntercalibration_NonClosure_highE__1down",
   "JET_EtaIntercalibration_NonClosure_highE__1up",
   "JET_EtaIntercalibration_NonClosure_negEta__1down",
   "JET_EtaIntercalibration_NonClosure_negEta__1up",
   "JET_EtaIntercalibration_NonClosure_posEta__1down",
   "JET_EtaIntercalibration_NonClosure_posEta__1up",
   "JET_EtaIntercalibration_TotalStat__1down",
   "JET_EtaIntercalibration_TotalStat__1up",
   "JET_Flavor_Composition__1down",
   "JET_Flavor_Composition__1up",
   "JET_Flavor_Response__1down",
   "JET_Flavor_Response__1up",
   "JET_Pileup_OffsetMu__1down",
   "JET_Pileup_OffsetMu__1up",
   "JET_Pileup_OffsetNPV__1down",
   "JET_Pileup_OffsetNPV__1up",
   "JET_Pileup_PtTerm__1down",
   "JET_Pileup_PtTerm__1up",
   "JET_Pileup_RhoTopology__1down",
   "JET_Pileup_RhoTopology__1up",
   "JET_PunchThrough_MC21__1down",
   "JET_PunchThrough_MC21__1up",
   "JET_SingleParticle_HighPt__1down",
   "JET_SingleParticle_HighPt__1up",
   "JET_JER_DataVsMC_MC21__1down",
   "JET_JER_DataVsMC_MC21__1down_PseudoData",
   "JET_JER_DataVsMC_MC21__1up",
   "JET_JER_DataVsMC_MC21__1up_PseudoData",
   "JET_JER_EffectiveNP_1__1down",
   "JET_JER_EffectiveNP_1__1up",
   "JET_JER_EffectiveNP_2__1down",
   "JET_JER_EffectiveNP_2__1up",
   "JET_JER_EffectiveNP_3__1down",
   "JET_JER_EffectiveNP_3__1up",
   "JET_JER_EffectiveNP_4__1down",
   "JET_JER_EffectiveNP_4__1up",
   "JET_JER_EffectiveNP_5__1down",
   "JET_JER_EffectiveNP_5__1up",
   "JET_JER_EffectiveNP_6__1down",
   "JET_JER_EffectiveNP_6__1up",
   "JET_JER_EffectiveNP_7__1down",
   "JET_JER_EffectiveNP_7__1up",
   "JET_JER_EffectiveNP_8__1down",
   "JET_JER_EffectiveNP_8__1up",
   "JET_JER_EffectiveNP_9__1down",
   "JET_JER_EffectiveNP_9__1up",
   "JET_JER_EffectiveNP_10__1down",
   "JET_JER_EffectiveNP_10__1up",
   "JET_JER_EffectiveNP_11__1down",
   "JET_JER_EffectiveNP_11__1up",
   "JET_JER_EffectiveNP_12restTerm__1down",
   "JET_JER_EffectiveNP_12restTerm__1up",
   "JET_JER_EffectiveNP_1__1down_PseudoData",
   "JET_JER_EffectiveNP_1__1up_PseudoData",
   "JET_JER_EffectiveNP_2__1down_PseudoData",
   "JET_JER_EffectiveNP_2__1up_PseudoData",
   "JET_JER_EffectiveNP_3__1down_PseudoData",
   "JET_JER_EffectiveNP_3__1up_PseudoData",
   "JET_JER_EffectiveNP_4__1down_PseudoData",
   "JET_JER_EffectiveNP_4__1up_PseudoData",
   "JET_JER_EffectiveNP_5__1down_PseudoData",
   "JET_JER_EffectiveNP_5__1up_PseudoData",
   "JET_JER_EffectiveNP_6__1down_PseudoData",
   "JET_JER_EffectiveNP_6__1up_PseudoData",
   "JET_JER_EffectiveNP_7__1down_PseudoData",
   "JET_JER_EffectiveNP_7__1up_PseudoData",
   "JET_JER_EffectiveNP_8__1down_PseudoData",
   "JET_JER_EffectiveNP_8__1up_PseudoData",
   "JET_JER_EffectiveNP_9__1down_PseudoData",
   "JET_JER_EffectiveNP_9__1up_PseudoData",
   "JET_JER_EffectiveNP_10__1down_PseudoData",
   "JET_JER_EffectiveNP_10__1up_PseudoData",
   "JET_JER_EffectiveNP_11__1down_PseudoData",
   "JET_JER_EffectiveNP_11__1up_PseudoData",
   "JET_JER_EffectiveNP_12restTerm__1down_PseudoData",
   "JET_JER_EffectiveNP_12restTerm__1up_PseudoData",
   "EG_RESOLUTION_ALL__1down",
   "EG_RESOLUTION_ALL__1up",
   "EG_SCALE_AF2__1down",
   "EG_SCALE_AF2__1up",
   "EG_SCALE_ALL__1down",
   "EG_SCALE_ALL__1up",
   "MET_SoftTrk_ResoPara",
   "MET_SoftTrk_ResoPerp",
   "MET_SoftTrk_ScaleDown",
   "MET_SoftTrk_ScaleUp",
   "MUON_CB__1down",
   "MUON_CB__1up",
   #"MUON_ID__1down",
   #"MUON_ID__1up",
   #"MUON_MS__1down",
   #"MUON_MS__1up",
   "MUON_SAGITTA_DATASTAT__1down",
   "MUON_SAGITTA_DATASTAT__1up",
   "MUON_SAGITTA_RESBIAS__1down",
   "MUON_SAGITTA_RESBIAS__1up",
   #"MUON_SAGITTA_RHO__1down",
   #"MUON_SAGITTA_RHO__1up",
   "MUON_SCALE__1down",
   "MUON_SCALE__1up",

]

systematics_btagging_DL1_77 = [
   "bTagSF_DL1dv01_77_eigenvars_B_down",
   "bTagSF_DL1dv01_77_eigenvars_B_up",
   "bTagSF_DL1dv01_77_eigenvars_C_down",
   "bTagSF_DL1dv01_77_eigenvars_C_up",
   "bTagSF_DL1dv01_77_eigenvars_Light_down",
   "bTagSF_DL1dv01_77_eigenvars_Light_up",
   "bTagSF_DL1dv01_77_extrapolation_down",
   "bTagSF_DL1dv01_77_extrapolation_up",
   "bTagSF_DL1dv01_77_extrapolation_from_charm_down",
   "bTagSF_DL1dv01_77_extrapolation_from_charm_up",
]


systematics_weight = [
   "jvt_DOWN",
   "jvt_UP",
   "pileup_DOWN",
   "pileup_UP",
   "indiv_SF_EL_ChargeID",
   "indiv_SF_EL_ChargeID_DOWN",
   "indiv_SF_EL_ChargeID_UP",
   "indiv_SF_EL_ChargeMisID",
   "indiv_SF_EL_ChargeMisID_STAT_DOWN",
   "indiv_SF_EL_ChargeMisID_STAT_UP",
   "indiv_SF_EL_ChargeMisID_SYST_DOWN",
   "indiv_SF_EL_ChargeMisID_SYST_UP",
   "indiv_SF_EL_ID_DOWN",
   "indiv_SF_EL_ID_UP",
   "indiv_SF_EL_Isol_DOWN",
   "indiv_SF_EL_Isol_UP",
   "indiv_SF_EL_Reco_DOWN",
   "indiv_SF_EL_Reco_UP",
   "indiv_SF_EL_Trigger_DOWN",
   "indiv_SF_EL_Trigger_UP",
   "indiv_SF_MU_ID_STAT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_UP",
   "indiv_SF_MU_ID_STAT_UP",
   "indiv_SF_MU_ID_SYST_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_UP",
   "indiv_SF_MU_ID_SYST_UP",
   "indiv_SF_MU_Isol_STAT_DOWN",
   "indiv_SF_MU_Isol_STAT_UP",
   "indiv_SF_MU_Isol_SYST_DOWN",
   "indiv_SF_MU_Isol_SYST_UP",
   "indiv_SF_MU_TTVA_STAT_DOWN",
   "indiv_SF_MU_TTVA_STAT_UP",
   "indiv_SF_MU_TTVA_SYST_DOWN",
   "indiv_SF_MU_TTVA_SYST_UP",
   "indiv_SF_MU_Trigger_STAT_DOWN",
   "indiv_SF_MU_Trigger_STAT_UP",
   "indiv_SF_MU_Trigger_SYST_DOWN",
   "indiv_SF_MU_Trigger_SYST_UP",
   "leptonSF_EL_SF_ID_DOWN",
   "leptonSF_EL_SF_ID_UP",
   "leptonSF_EL_SF_Isol_DOWN",
   "leptonSF_EL_SF_Isol_UP",
   "leptonSF_EL_SF_Reco_DOWN",
   "leptonSF_EL_SF_Reco_UP",
   "leptonSF_EL_SF_Trigger_DOWN",
   "leptonSF_EL_SF_Trigger_UP",
   "leptonSF_MU_SF_ID_STAT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_UP",
   "leptonSF_MU_SF_ID_STAT_UP",
   "leptonSF_MU_SF_ID_SYST_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_UP",
   "leptonSF_MU_SF_ID_SYST_UP",
   "leptonSF_MU_SF_Isol_STAT_DOWN",
   "leptonSF_MU_SF_Isol_STAT_UP",
   "leptonSF_MU_SF_Isol_SYST_DOWN",
   "leptonSF_MU_SF_Isol_SYST_UP",
   "leptonSF_MU_SF_TTVA_STAT_DOWN",
   "leptonSF_MU_SF_TTVA_STAT_UP",
   "leptonSF_MU_SF_TTVA_SYST_DOWN",
   "leptonSF_MU_SF_TTVA_SYST_UP",
   "leptonSF_MU_SF_Trigger_STAT_DOWN",
   "leptonSF_MU_SF_Trigger_STAT_UP",
   "leptonSF_MU_SF_Trigger_SYST_DOWN",
   "leptonSF_MU_SF_Trigger_SYST_UP",
   ]


#####################

known_channels = [ "emu","ee","mumu" ]

known_regions = ["incl",
                 "0jex0bex",
                 "1jin0bex", "1jin1bin"
#                 "1bex",
#                 "2bex", 
#                 "3bin",
                ]
 
##################### 
#PROFILE LIKELIHOOD HISTOS
profile_likelihood_histo = {}
histograms = {}
for channel in known_channels:
   histograms[channel] = {}
   profile_likelihood_histo[channel] = {}
   profile_likelihood_histo[channel]['h_OS_ge0b'] =  TH1F("h_%s_OS_ge0b"%(channel),"h_%s_OS_ge0b"%(channel),1,0,1)
   profile_likelihood_histo[channel]['h_OS_1b'] =    TH1F("h_%s_OS_1b"%(channel),  "h_%s_OS_1b"%(channel),1,0,1)
   profile_likelihood_histo[channel]['h_OS_2b'] =    TH1F("h_%s_OS_2b"%(channel),  "h_%s_OS_2b"%(channel),1,0,1)
   for region in known_regions:
      histograms[channel][region] = {}


      histograms[channel][region]['el_pt']   = TH1F( "%s_%s_el_pt"%(channel,region),   ";Electron p_{T} [GeV]"    , 23, 24., 300. )
      histograms[channel][region]['mu_pt']   = TH1F( "%s_%s_mu_pt"%(channel,region),   ";Muon p_{T} [GeV]"        , 23, 24., 300. )

      histograms[channel][region]['el_eta']  = TH1F( "%s_%s_el_eta"%(channel,region),  ";Electron #eta"   , 50, -2.5, 2.5 )
      histograms[channel][region]['muon_eta']  = TH1F( "%s_%s_muon_eta"%(channel,region),  ";Muon #eta"   , 50, -2.5, 2.5 )

      histograms[channel][region]['met']  = TH1F( "%s_%s_met"%(channel,region),  ";E_{T}^{miss} [GeV]", 50, 0., 300. )
      histograms[channel][region]['HT']   = TH1F( "%s_%s_HT"%(channel,region),   ";HT [GeV]" , 100, 0., 1500.  )

      histograms[channel][region]['j1_pt']    = TH1F( "%s_%s_j1_pt"%(channel,region), ";1st jet p_{T} [GeV]", 50, 0., 500. )
      histograms[channel][region]['j1_eta']   = TH1F( "%s_%s_j1_eta"%(channel,region),";1st jet #eta", 50, -2.5, 2.5 )

      histograms[channel][region]['bj1_pt']    = TH1F( "%s_%s_bj1_pt"%(channel,region), ";1st b-jet p_{T} [GeV]", 50, 0., 500. )
      histograms[channel][region]['bj1_eta']   = TH1F( "%s_%s_bj1_eta"%(channel,region),";1st b-jet #eta", 50, -2.5, 2.5 )

      histograms[channel][region]['Z_m']       = TH1F( "%s_%s_Z_m"%(channel,region), ";m(ll) [GeV]", 25, 0., 250. )

      ## njets
      histograms[channel][region]['jets_N']   = TH1F( "%s_%s_jets_n"%(channel,region),  ";Jets multiplicity", 7, -0.5, 6.5 )
      histograms[channel][region]['bjets_N']  = TH1F( "%s_%s_bjets_n"%(channel,region), ";b-jets multiplicity"  , 4, -0.5, 3.5 )


      for h in histograms[channel][region].values(): 
         h.Sumw2()


#for h in profile_likelihood_histo.values(): h.Sumw2()
  
  
#####################
