## copy of helper_functions_1312_NOMUONCRACK 

from pickle import FALSE, TRUE
import numpy as np
from common import *
from ROOT import *
from math import cos, sin, sqrt, pow
from lxml import etree as ET
import os

#####################
Dir = os.path.dirname(os.path.abspath(__file__))
#xmlFname = Dir + "/xml/SumW.all.short.2L.xml"
#xmlFname = Dir + "/xml/SumW.all.ttbar.p5057.xml" # Used for all nominal+syst p5057 computations
#xmlFname = Dir + "/xml/SumW.run3.all.xml"
xmlFname = Dir + "/xml/SumW.all.test.xml" # Dummy value for CI pipeline
XMLtree = ET.parse(xmlFname) #Hard coded, can be updated w/ CreateXml.py
def ReadXmlTree( syst, dsid ):
	try:  
		CorrFactor = float(XMLtree.find("./MCInfo[@DSID='%s']/Systematic[@OurSystName='%s']" % (dsid, syst)).attrib['CorrFactor']) #Grab correction factor for correct SumW renormalization
		Idx = int(XMLtree.find("./MCInfo[@DSID='%s']/Systematic[@OurSystName='%s']" % (dsid, syst)).attrib['GeneratorIdx']) #Index in mc_generator_weights corresponding to systematic
		return CorrFactor, Idx
	except AttributeError: return False, False

#####################

def GetEventWeight( ele, muon, tree, syst, b_tag_cut, dsid):
    CorrectionFactor, Index = ReadXmlTree(syst, dsid)
    w_extra = 1
    #print("Correction factor: ", CorrectionFactor, "Idx: ", Index)

    if Index != False:
        wIdx = tree.mc_generator_weights[Index]
        #print "Index: ", Index
        #print "mc_generator_weights[Index]: ", wIdx
        #print "mc_generator_weights[0]: ", tree.mc_generator_weights[0]

        if wIdx == 0:
            print "Exceptional WARNING!!! the value of index %s is: " %  (Index), wIdx
            print "DEBUG: Event number = %-10i run number = %-10i " % ( tree.eventNumber, tree.runNumber )
        w_extra = CorrectionFactor * wIdx / tree.mc_generator_weights[0]
        #print "w_extra: ", w_extra


    w = 1.
    NomSumW = float(XMLtree.find("./MCInfo[@DSID='%s']" % dsid).attrib['NominalSumW'])
    #print "Nominal Sum of weights: ", NomSumW

    # Rel.22 let's multiply it by the lepton, pileup, JVT and btagging weights already here
    w_leptonSF = tree.weight_leptonSF
    #w_trigger  = tree.weight_trigger
    w_trigger = 1.
    w_beamspot   = tree.weight_beamspot
    w_jvt      = tree.weight_jvt
    w_pileup  =   tree.weight_pileup

    if   b_tag_cut==60 : w_btag = tree.weight_bTagSF_DL1dv01_60 
    elif b_tag_cut==70 : w_btag = tree.weight_bTagSF_DL1dv01_70
    elif b_tag_cut==77 : w_btag = tree.weight_bTagSF_DL1dv01_77
    elif b_tag_cut==85 : w_btag = tree.weight_bTagSF_DL1dv01_85
    else:
       print(">>>> ERROR: please specify a b_tag_cut!!!!!!")
  
    
    if syst == "nominal":
        pass
    
    elif syst in [ "pileup_UP", "pileup_DOWN" ]:
        w_pileup = tree.weight_pileup_UP if syst == "pileup_UP" else tree.weight_pileup_DOWN # change w_pileup by the UP/DOWN
        #print "CHECK: in syst=pileup_UP, w_pileup: ", w_pileup

    elif syst in [ "jvt_UP", "jvt_DOWN" ]:
        w_jvt = tree.weight_jvt_UP if syst == "jvt_UP" else tree.weight_jvt_DOWN # change w_jvt by the UP/DOWN variations
        #print "CHECK: in syst=jvt_UP, w_jvt: ", w_jvt
        
    elif syst.startswith("bTagSF"):
        if "eigenvars" in syst:
            k = int( syst.split('_')[-1] )
            syst_btag   = syst.replace( "_up_%i"%k, "_up" ).replace( "_down_%i"%k, "_down" )
            syst_branch = "weight_%s" % syst_btag
            #print "CHECK: in syst=btagSF eigenvars, syst_branch: ", syst_branch
            exec( "w_btag = tree.%s[%i]" % (syst_branch, k ) )   # change w_btag by the UP/DOWN variations
            #print "CHECK: in syst=btagSF, w_btag: ", w_btag

            
        else:
            syst_branch = "weight_%s" % syst
            #print "CHECK: in syst=btagSF extrapolation, syst_branch: ", syst_branch	
            exec( "w_btag = tree.%s" % syst_branch )
            #print "CHECK: in syst=btagSF, w_btag: ", w_btag

            
    elif syst.startswith("leptonSF"):
        syst_branch = "weight_%s" % syst
        #print "CHECK: in syst=leptonSF, syst_branch: ", syst_branch	
        exec( "w_leptonSF = tree.%s" % syst_branch )
        #print "CHECK: in syst=leptonSF, w_leptonSF: ", w_leptonSF

    #ADDING NEW STUFF
    elif syst.startswith("trigger"):
        syst_branch = "weight_%s" % syst
        #print "CHECK: in syst=leptonSF, syst_branch: ", syst_branch	
        exec( "w_trigger = tree.%s" % syst_branch )
        #print "CHECK: in syst=leptonSF, w_leptonSF: ", w_leptonSF

  
   
    # print("---------------------------")
    # print "w_leptonSF: ", w_leptonSF
    # print "w_trigger: ", w_trigger
    # print "w_beamspot: ", w_beamspot
    # print "w_jvt: ", w_jvt
    # print "w_btag: ", w_btag
    # print "w_pileup: ", w_pileup
    # print "NomSumW: ", NomSumW
    #print(w_btag)
    w *= w_leptonSF * w_trigger * w_beamspot* w_jvt * w_btag * w_extra * w_pileup
    #print "w: ", w
    #print "w_extra: ", w_extra
    w /= NomSumW    
    return w, w_extra


#####################

def MakeEventElectron( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0], tree.el_e[0] )
    electron_mass =  0.51099895  # in MEV
    lep.SetPtEtaPhiM( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0],  electron_mass)
    lep.q = tree.el_charge[0]
    lep.flav = 11 * lep.q
  #  lep.topoetcone = tree.el_topoetcone20[0]
  #  lep.ptvarcone  = tree.el_ptvarcone20[0]
  #  lep.d0sig      = tree.el_d0sig[0]
  #  lep.ITrk       = tree.el_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.el_pt)
    lep.Crack      = False
    if 1.37 < abs(lep.Eta()) < 1.52: lep.Crack = True
    return lep

def MakeEventElectron2( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0], tree.el_e[0] )
    electron_mass =  0.51099895  # in MEV
    lep.SetPtEtaPhiM( tree.el_pt[1], tree.el_cl_eta[1], tree.el_phi[1],  electron_mass)
    lep.q = tree.el_charge[1]
    lep.flav = 11 * lep.q
  #  lep.topoetcone = tree.el_topoetcone20[0]
  #  lep.ptvarcone  = tree.el_ptvarcone20[0]
  #  lep.d0sig      = tree.el_d0sig[0]
  #  lep.ITrk       = tree.el_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.el_pt)
    lep.Crack      = False
    if 1.37 < abs(lep.Eta()) < 1.52: lep.Crack = True
    return lep

#~~~~~~~~~~~~

def MakeEventMuon( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], tree.mu_e[0] )
    muon_mass = 105.6583755 # in MeV   
    lep.SetPtEtaPhiM( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], muon_mass )
    lep.q = tree.mu_charge[0]
    lep.flav = 13 * lep.q
 #   lep.topoetcone = tree.mu_topoetcone20[0]
 #   lep.ptvarcone  = tree.mu_ptvarcone30[0]
 #   lep.d0sig      = tree.mu_d0sig[0]
 #   lep.ITrk       = tree.mu_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.mu_pt)
    lep.Crack      = False
    return lep


def MakeEventMuon2( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], tree.mu_e[0] )
    muon_mass = 105.6583755 # in MeV   
    lep.SetPtEtaPhiM( tree.mu_pt[1], tree.mu_eta[1], tree.mu_phi[1], muon_mass )
    lep.q = tree.mu_charge[1]
    lep.flav = 13 * lep.q
 #   lep.topoetcone = tree.mu_topoetcone20[0]
 #   lep.ptvarcone  = tree.mu_ptvarcone30[0]
 #   lep.d0sig      = tree.mu_d0sig[0]
 #   lep.ITrk       = tree.mu_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.mu_pt)
    lep.Crack      = False
    return lep

#~~~~~~~~~~~~~~~~~~
#### set cut on btagging DL1r value for 77% WP and the corresponding weigth is bTagSF_DL1r_77
def MakeEventJets( tree, jet_pt_cut, b_tag_cut):
    jets = []
    bjets = []

    #print "INFO: jet pT cut set to %f " % jet_pt_cut
    #print "INFO: jet WP cut set to %i " % b_tag_cut


    # this is in case we want to limit the number of jets
    max=999

    # in case is needed, we use up to the four highest pT jets
    jets_n = len( tree.jet_pt )
    #print "INFO: number of jets as length of jet_pt vector is: ", jets_n

    if jets_n > max : jets_n = max
    #print "INFO: number of jets used in MakeEventJets is: ", jets_n
       
    for i in range(jets_n):


        jet_pt = 0.

        # mass issue
        jet_before = TLorentzVector()
        jet_mass = 0.
        jet_before.SetPtEtaPhiE( tree.jet_pt[i], tree.jet_eta[i], tree.jet_phi[i], tree.jet_e[i])
        jet_mass = jet_before.M()

#        print "INFO: jet iterator i is: ", i
#        print "INFO: jet eta: ", tree.jet_eta[i]
#        print "INFO: jet pT: ", tree.jet_pt[i]/GeV
#        print "INFO: DL1r 60 is: ", float(ord(tree.jet_isbtagged_DL1dv01_60[i]))
#        print "INFO: 70 is: ",      float(ord(tree.jet_isbtagged_DL1dv01_70[i]))
#        print "INFO: 77 is: ",      float(ord(tree.jet_isbtagged_DL1dv01_77[i]))


        #  default value 
        jet_pt = tree.jet_pt[i]
        jet_eta = tree.jet_eta[i]

        # make a cut on jets of jet_pt_cut [in GeV]
        if jet_pt/GeV < jet_pt_cut: continue
       
        jets += [ TLorentzVector() ]

        ## now with mass instead of energy
        jets[-1].SetPtEtaPhiM( jet_pt, tree.jet_eta[i], tree.jet_phi[i], jet_mass )
       
        jets[-1].index = i

        # Tomas decided not to store this info
        # jets[-1].dl1 = tree.jet_DL1r[i]

        # general definition
        jets[-1].isBtagged = False

        # per WP definition
        # jets[-1].isbtagged_DL1r_85 = float(ord(tree.jet_isbtagged_DL1dv01_85[i]))
        jets[-1].isbtagged_DL1dv01_77 = float(ord(tree.jet_isbtagged_DL1dv01_77[i]))
        # jets[-1].isbtagged_DL1r_70 = float(ord(tree.jet_isbtagged_DL1dv01_70[i]))
        # jets[-1].isbtagged_DL1r_60 = float(ord(tree.jet_isbtagged_DL1dv01_60[i]))

        # Rel. 22 : make the cut on the value
        if jets[-1].isbtagged_DL1dv01_77 == 1 : 
            jets[-1].isBtagged = True
            bjets += [ TLorentzVector(jets[-1]) ]
            bjets[-1].index = i
            #print "INFO: bjet identified : ", bjets[-1].index



    return jets, bjets

def EventHasLeptonFakes(tree):

    for i in range(len(tree.el_pt)):  # Electron loop
        if hasattr(tree, 'el_true_type') & hasattr(tree, 'el_true_isPrompt'):
            if not tree.el_true_type.at(i) == 2: 
                return True
            if not tree.el_true_isPrompt.at(i): 
                return True
    
    for i in range(len(tree.mu_pt)):  # Muon loop
        if hasattr(tree, 'mu_true_type') & hasattr(tree, 'mu_true_isPrompt'):
            if not tree.mu_true_type.at(i) == 6: 
                return True
            if not tree.mu_true_isPrompt.at(i): 
                return True
    return False

#############################################################################################################################
