#!bin/sh


export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

setupATLAS

# check this version!
ARCH="x86_64-centos7-gcc8-opt"
#http://lcginfo.cern.ch/

lsetup "root 6.18.04-${ARCH}"

rm *.log

#lsetup "lcgenv -p LCG_96b ${ARCH} pyanalysis"
#lsetup "lcgenv -p LCG_96b ${ARCH} pytools"
