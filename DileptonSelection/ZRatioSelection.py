#!/usr/bin/env python
import os, sys, time
import argparse

timer_start = time.time()

from array import array
from numpy import median

import ROOT as rt

from common import *
import helper_functions

########################################################
parser = argparse.ArgumentParser(description='ttbar 13.6 TeV: Dilepton selection maker')
parser.add_argument( '-i', '--filelistname', default="filelists/check.txt" )
parser.add_argument( '-o', '--outfilename',  default="" )
parser.add_argument( '-f', '--fakes',        action='store_true', default=False )
parser.add_argument( '-s', '--systematic',   default="nominal" )
parser.add_argument( '-p', '--preselection', default="check" )
parser.add_argument( '-j', '--jetcut',       default="25" )    # default is 20 GeV
parser.add_argument( '-HF', '--Wjets_HF' ,   default="all" )   # l, c, b, 1bTruth, 2bTruth

#### we are using 77% wp now
parser.add_argument( '-b', '--btagcut',      default="77" )    # we are cutting on DL1r, see selection file JET_N_BTAG DL1r:FixedCutBEff_77 >= 1

args         = parser.parse_args()
filelistname = args.filelistname
outfilename  = args.outfilename
syst         = args.systematic
preselection = args.preselection
jetcut       = args.jetcut
btagcut      = args.btagcut
Wjets_HF     = args.Wjets_HF
fakes        = args.fakes

# check what is data and what is not
isData = False
if filelistname.find('data') > -1: isData = True

isQCD = False
if filelistname.find('qcd') > -1:
    isData = True
    isQCD = True

isWjets = False
if filelistname.find('Wjets') > -1: isWjets = True



# check I am reading a filelist
if filelistname == "":
    print("ERROR: please specify a file list")
    exit(1)

# default name of the tree
treename = "nominal"

## here begins the systs change, in case it is not nominal
if not isData:
    # search in common.py for all the systs defined in systematics_tree
    if syst in systematics_tree:
        treename = syst # change the name of the tree to be read
    else:
        treename = "nominal"

if isQCD:
    treename = "nominal_Loose"

print "INFO: running filelistname: ", filelistname
print "INFO: running preselection: ", preselection
print "INFO: running systematic  : ", syst
print "INFO: reading TTree                               : ", treename
print "INFO: using jet pT cut                            : ", float(jetcut)
print "INFO: using jet WP cut                            : ", float(btagcut)
if isWjets:
    print "INFO: using W+jets HF (l=light, c or b-jets, or truth jets) cut    : ", Wjets_HF



################################################################################

# Get ROOT trees
tree = rt.TChain( treename, treename )
for fname in open( filelistname, 'r' ).readlines():
    fname = fname.strip()
    tree.Add( fname )


# output name
ext = filelistname.split('/')[-1].split('.')[-1]

# need to check what happens for systs in Wjets splitting
if outfilename == "":
    outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.root"%syst)
    if isWjets:
        outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.%s.root"%(Wjets_HF,syst))

    if syst == "nominal":
        outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "root")
        if isWjets:
            outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.root"%Wjets_HF)


############ 
if fakes:
    # Create outfile for fakes
    outfilename = outfilename.replace( ".root", "_fakes.root")
    outfile = rt.TFile.Open( outfilename, "RECREATE" )
else:
    outfile = rt.TFile.Open( outfilename, "RECREATE" )

print "INFO: output saved  in                            : ", outfilename

############# 


# Event loop
nentries = tree.GetEntries()

# lower number of events
#nentries=500

print "INFO: looping over %i entries" % nentries


for ientry in range(nentries):
    tree.GetEntry( ientry )
    #print("-------------------------------------")
    #print "INFO: Event %-9i " % ( ientry)

    # print INFO
    eventNumber     = tree.eventNumber
    runNumber       = tree.runNumber
    mcChannelNumber = tree.mcChannelNumber

    if ( nentries < 10 ) or ( (ientry+1) % int(float(nentries)/10.)                == 0 ):
        perc = 100. * ientry / float(nentries)
        print "INFO: Event %-9i (event number = %-10i run number = %-10i )   (%3.0f %%)" % ( ientry, eventNumber, runNumber, perc )

    jet_pt_cut     = float(jetcut)
    b_tag_cut      = float(btagcut)

    # check pre-selection, watch out as I only ask 2018 here
    channel = 0

    try:
        passed_emu_2018  = tree.emu_2018      
        if passed_emu_2018 == 1: channel = 1
    except:
        pass 
    try:
        passed_ee_2018  = tree.ee_2018   
        if passed_ee_2018 == 1: channel = 2
    except:
        pass    
    
    try:
        passed_mumu_2018  = tree.mumu_2018   
        if passed_mumu_2018 == 1: channel = 3   
    except:
        pass 

    accepted = ( channel > 0 )
    if not accepted: continue


    # Identify lepton
    ch_label = ""
    
    # if helper_functions.EventHasLeptonFakes( tree ):
    #     ch_label += "fakes"

    if channel == 1:
      ele  = helper_functions.MakeEventElectron( tree )   
      muon = helper_functions.MakeEventMuon( tree )
      ch_label += "emu"

    if channel == 2:
      ele  = helper_functions.MakeEventElectron( tree )   
      muon = helper_functions.MakeEventElectron2( tree )
      ch_label += "ee"

    if channel == 3:
      ele  = helper_functions.MakeEventMuon( tree )   
      muon = helper_functions.MakeEventMuon2( tree )
      ch_label += "mumu"


    # 2 opposite charge leptons
    pass2OSL = ( ele.q * muon.q < 0 )
    if not pass2OSL: continue


    # cut on lepton pT 
    if ele.Pt()/GeV < 25: continue
    if muon.Pt()/GeV < 25: continue

    # Check for fakes option
    # if fakes == False:
    #     if helper_functions.EventHasLeptonFakes( tree ):
    #         continue
    # else:
    #     if not helper_functions.EventHasLeptonFakes( tree ):
    #         continue
   
  
    ###################################################################################################
    # syst and nominal have different weights stored in tree!
    # if syst is not specified, syst=nominal
    w = 1.
    if not mcChannelNumber == 0:
        dsid = str(tree.mcChannelNumber) # read the DSID
        w *= tree.weight_mc  # multiply by MC weight by default

        w_total, w_extra = helper_functions.GetEventWeight( ele, muon, tree, syst, b_tag_cut, dsid ) 

        w *= w_total

        w *= iLumi * xs[dsid] # multiply by luminosity and by the cross-section of the sample


    ###################################################################################################
    # QCD magic (taken from MC later) 

    ###################################################################################################
    # regions
    jets_n  = len(tree.jet_pt)
    n_bjets = len(tree.jet_pt)

    ############################################################
    jets, bjets = helper_functions.MakeEventJets( tree, jet_pt_cut,  b_tag_cut)

    jets_n  = len( jets )
    n_bjets = len( bjets )

    #print "INFO: selection on jets: Njets = %i and Nbjets = %i"  % ( jets_n, n_bjets )
    
    # cut on nJets 
    #if jets_n < 4: continue
    #if n_bjets != 2: continue


    ###################################################################################################
    # MET
    MET = tree.met_met

    # l1+l2 lorentz vector
    Z = ele + muon

    # The invariant mass of the lepton pair required to satisfy m(ll) > 15 GeV
    if Z.M() < 75*GeV |  Z.M() > 105*GeV: continue

    # print "INFO: Event number = %-10i run number = %-10i " % ( eventNumber, runNumber )
    # print "INFO: selection on jets: Njets = %i and Nbjets = %i"  % ( jets_n, n_bjets )
    # print "INFO: Electron pt = %f, eta = %f, phi = %f"                % ( ele.Pt()/GeV, ele.Eta(), ele.Phi() )
    # print "INFO: Muon     pt = %f, eta = %f, phi = %f"                % ( muon.Pt()/GeV, muon.Eta(), muon.Phi() )
    # print "INFO: Z(ll) = %f and MET = %f"                % ( Z.M()/GeV, MET/GeV )


    ###################################################################################################
    # decide region
    #region = "incl"
    region = ""

    if jets_n == 0 : region += "0jex"
    if jets_n > 0 : region += "1jin"

    if n_bjets == 0 : region += "0bex"
    if n_bjets > 0 : region += "1bin"
    #print "INFO: region name is: ", region


    ###################################################################################################
    # scalar pT sum of all selected jets and leptons!
    HT = 0.
    HT += ele.Pt()
    HT += muon.Pt()

    if jets_n >= 1:
       for j in jets:
          HT += j.Pt()

    ##################################### BEGIN SAVING HISTOGRAMS ##################################### 

    # begin saving plots
    #for channel in [ "emu", ch_label ]:


    for channel in [ ch_label ]:
        histograms[channel][region]['el_pt'].Fill( ele.Pt()/GeV, w ) 
        histograms[channel][region]['mu_pt'].Fill( muon.Pt()/GeV, w ) 

        histograms[channel][region]['el_eta'].Fill( ele.Eta(), w )
        histograms[channel][region]['muon_eta'].Fill( muon.Eta(), w )

        histograms[channel][region]['met'].Fill( MET/GeV, w )
        histograms[channel][region]['HT'].Fill( HT/GeV, w )

        if n_bjets >= 0:
            profile_likelihood_histo[channel]['h_OS_ge0b'].Fill(0,w)

        if n_bjets == 1:
            profile_likelihood_histo[channel]['h_OS_1b'].Fill(0,w)

        if n_bjets == 2:
            profile_likelihood_histo[channel]['h_OS_2b'].Fill(0,w)
        
        if jets_n >= 1:
            histograms[channel][region]['j1_pt'].Fill( jets[0].Pt()/GeV, w )
            histograms[channel][region]['j1_eta'].Fill( jets[0].Eta(), w )

        if n_bjets>= 1:
            histograms[channel][region]['bj1_pt'].Fill( bjets[0].Pt()/GeV, w )
            histograms[channel][region]['bj1_eta'].Fill( bjets[0].Eta(), w )

        histograms[channel][region]['Z_m'].Fill( Z.M()/GeV, w ) 

        histograms[channel][region]['jets_N'].Fill( jets_n, w )
        histograms[channel][region]['bjets_N'].Fill( n_bjets, w)

# Write out
outfile.cd()
for channel in known_channels:
    for region in known_regions:
        # add overflow bin
        for h in histograms[channel][region].values() :
            if(h.GetBinContent(h.GetNbinsX()+1) > 0) :
                h.AddBinContent(h.GetNbinsX(), h.GetBinContent(h.GetNbinsX()+1))
        # save
        for h in histograms[channel][region].values(): h.Write()


    for h in profile_likelihood_histo[channel].values(): h.Write()


#save file
outfile.Close()
print "INFO: output file created", outfilename
