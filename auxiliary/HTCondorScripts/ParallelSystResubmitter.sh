#!/bin/bash
Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-n|-c|-p|-o|-h]"
   echo "options:"
   echo "-n     Name of the output folder (normally \"syst\")"
   echo "-c     Name of the channel: 1L or 2L"
   echo "-s     Name of the systematic type (weights, tree)"
   echo "-p     Path to the filelist, e.g. /eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/dilepton/ntuples_ATR22_Apr2022_mc20e/mc/ttbar_modelling/"
   echo "-h     Print this Help."
   echo
}

#RESET THE LAST OPTION PROCESSED
OPTIND=1

while getopts o:n:c:s:p:h flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        c) channel=${OPTARG};;
        p) dirpath=${OPTARG};;
        s) syst_type=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            return 1;;
		\?) echo 'invalid option!' >&2
            return 1;;
		
    esac
done


#!/bin/bash

#RESET THE LAST OPTION PROCESSED
OPTIND=1

if [ -z $1 ]
then
    echo -e "ERROR\t Please provide the path to the syst submission file."
    return 1
fi

echo -e "INFO\t Searching for syst submission file.."
if [ -f $1 ]
then
    echo -e "INFO\t Reading syst submission file.."
else
    echo -e "ERROR\t Couldn't find syst submission file. Try again."
    return 1
fi

# Initialize Jobs
###################################################################################################################################################

resubmission_list=$1_resub.txt

echo -e "INFO\t Cleaning up previous resubmission.."
if [ -f $resubmission_list ]
then
    rm -rf $resubmission_list
fi

while IFS= read -r line
do
    IFS=',' read -r -a filename <<< "$line"

    # Select just the name of the file, not the path and remove the 
    IFS='/' read -r -a logfile <<< "$1"
    good_prefix=${logfile[-1]}
    # good_prefix="${logfile[1]#w_}"
    # good_prefix="${good_prefix#t_}"

    #echo $good_prefix

    filenamepath=condor/err/${good_prefix}_${filename[0]}.err
    #echo $filenamepath
    if [ -f "$filenamepath" ]; then
        if [ -s "$filenamepath" ]; then
            # The file is not-empty.
            echo "$line" >> $resubmission_list
        else
            # The file is empty.
            continue
        fi
    else
        echo "$line" >> $resubmission_list
    fi

done < $1

if [ -f $resubmission_list ]
then
    # Submit the jobs
    #csub -n ${today}_parallel_conversion -m -p -j parent_process_list.txt
    #echo "Submitting this: csub -n parallel_conversion -j resubmission_list.txt"
    csub -n $good_prefix -j $resubmission_list
else
    echo -e "INFO\t All jobs completed successfully, no need for me :D"
fi
