#!/bin/bash
Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-p|-o|-h]"
   echo "options:"
   echo "-p     Path to the folder containing the datasets, e.g. /gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/SingleLeptonSelection/condor/err/"
   echo "-h     Print this Help."
   echo
}

#RESET THE LAST OPTION PROCESSED
OPTIND=1
outdir=`pwd`

while getopts o:p:h flag
do
    case "${flag}" in
        p) dirpath=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            exit 1;;
		\?) echo 'invalid option!' >&2
            exit 1;;
		
    esac
done

if [ -z $dirpath ]; then
    echo "ERROR: missing mandatory argument! Run ./MakeFileList.sh -h for help."
    exit 1
fi 


today=$(date +'%d_%m_%Y')
now=$(date +"%H-%M")

here=`pwd`

outfile=${outdir}/error_log_${today}_${now}.txt

printf "\r %-20s %-20s\n" DATE FILENAME | tee $outfile

echo "${outdir}/error_log_${today}_${now}.txt"

for filename in `ls -t ${dirpath}/*.err`; do
    
    #cmd=$(ll $filename)
    IFS=' ' read -ra ADDR <<< "$(ll $filename)"
    date="${ADDR[5]}-${ADDR[6]}-${ADDR[7]}"
    
    name=$here/${ADDR[8]}


    if [ -s "$filename" ]; then
        # The file is not-empty.
        printf "\r %-20s %-20s\n" $date $name | tee -a $outfile
    else
        # The file is empty.
        continue
    fi

done
