#!/bin/bash
Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-n|-c|-p|-o|-h]"
   echo "options:"
   echo "-n     Name of the output folder (could be \"syst\" for instance)"
   echo "-c     Name of the channel: 1L or 2L"
   echo "-s     Name of the systematic type (weights, tree)"
   echo "-p     Path to the filelist, e.g. filelists/mc.ttbar.nominal.txt"
   echo "-h     Print this Help."
   echo
}

#RESET THE LAST OPTION PROCESSED
OPTIND=1

while getopts o:n:c:s:p:h flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        c) channel=${OPTARG};;
        p) dirpath=${OPTARG};;
        s) syst_type=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            return 1;;
		\?) echo 'invalid option!' >&2
            return 1;;
		
    esac
done


if [ -z $dirpath ] || [ -z $name ] || [ -z $channel ]; then
    echo -e "ERROR\t missing a mandatory argument! Run -h for help."
    return 1
fi 

if [ ! -d $name ]; then
    mkdir -p output/$name
fi

## submit to the batch script
here=`pwd`

###################################
# output name, DONT FORGET TO CREATE IT BY HAND BEFORE RUNNING!!!!!!!!!
#name="ttbar_test_0"

if [ -d output/$name ];then
      echo -e "INFO\t output folder ok"
else
      mkdir -p output/$name
      echo -e "INFO\t output folder created"
fi

if [ -d syst_sub_folder ];then
      echo -e "INFO\t sub files storage folder ok"
else
      mkdir -p syst_sub_folder
      echo -e "INFO\t sub files storage folder created"
fi

###################################
# only ttbar ==> linked to mc.ttbarNomSyst.txt

#w/zjets
systematicsSCALES_wzjets="muR0.5_muF0.5 muR0.5_muF1.0 muR1.0_muF0.5 muR1.0_muF1.0 muR1.0_muF2.0 muR2.0_muF1.0 muR2.0_muF2.0"

# weights
systematicsSCALES="Scale_muF_UP Scale_muF_DOWN Scale_muR_UP Scale_muR_DOWN Var3c_UP Var3c_DOWN FSR_UP FSR_DOWN"

# PDF
systematicsPDF="ttbar_PDF_MMHT ttbar_PDF_CT14 ttbar_PDF_PDF4LHC ttbar_PDF_1 ttbar_PDF_2 ttbar_PDF_3 ttbar_PDF_4 ttbar_PDF_5 ttbar_PDF_6 ttbar_PDF_7 ttbar_PDF_8 ttbar_PDF_9 ttbar_PDF_10 ttbar_PDF_11 ttbar_PDF_12 ttbar_PDF_13 ttbar_PDF_14 ttbar_PDF_15 ttbar_PDF_16 ttbar_PDF_17 ttbar_PDF_18 ttbar_PDF_19 ttbar_PDF_20 ttbar_PDF_21 ttbar_PDF_22 ttbar_PDF_23 ttbar_PDF_24 ttbar_PDF_25 ttbar_PDF_26 ttbar_PDF_27 ttbar_PDF_28 ttbar_PDF_29 ttbar_PDF_30"

## systematics that are stored as weights in files and correspond to btagging  
systematicsBTAG="bTagSF_DL1r_77_eigenvars_B_down_0 bTagSF_DL1r_77_eigenvars_B_down_1 bTagSF_DL1r_77_eigenvars_B_down_2 bTagSF_DL1r_77_eigenvars_B_down_3 bTagSF_DL1r_77_eigenvars_B_down_4 bTagSF_DL1r_77_eigenvars_B_down_5 bTagSF_DL1r_77_eigenvars_B_down_6 bTagSF_DL1r_77_eigenvars_B_down_7 bTagSF_DL1r_77_eigenvars_B_down_8 bTagSF_DL1r_77_eigenvars_B_up_0 bTagSF_DL1r_77_eigenvars_B_up_1 bTagSF_DL1r_77_eigenvars_B_up_2 bTagSF_DL1r_77_eigenvars_B_up_3 bTagSF_DL1r_77_eigenvars_B_up_4 bTagSF_DL1r_77_eigenvars_B_up_5 bTagSF_DL1r_77_eigenvars_B_up_6 bTagSF_DL1r_77_eigenvars_B_up_7 bTagSF_DL1r_77_eigenvars_B_up_8 bTagSF_DL1r_77_eigenvars_C_down_0 bTagSF_DL1r_77_eigenvars_C_down_1 bTagSF_DL1r_77_eigenvars_C_down_2 bTagSF_DL1r_77_eigenvars_C_down_3 bTagSF_DL1r_77_eigenvars_C_up_0 bTagSF_DL1r_77_eigenvars_C_up_1 bTagSF_DL1r_77_eigenvars_C_up_2 bTagSF_DL1r_77_eigenvars_C_up_3 bTagSF_DL1r_77_eigenvars_Light_down_0 bTagSF_DL1r_77_eigenvars_Light_down_1 bTagSF_DL1r_77_eigenvars_Light_down_2 bTagSF_DL1r_77_eigenvars_Light_down_3 bTagSF_DL1r_77_eigenvars_Light_up_0 bTagSF_DL1r_77_eigenvars_Light_up_1 bTagSF_DL1r_77_eigenvars_Light_up_2 bTagSF_DL1r_77_eigenvars_Light_up_3 bTagSF_DL1r_77_extrapolation_down bTagSF_DL1r_77_extrapolation_up bTagSF_DL1r_77_extrapolation_from_charm_down bTagSF_DL1r_77_extrapolation_from_charm_up"

#Old tree syst
#"CategoryReduction_JET_BJES_Response__1down CategoryReduction_JET_BJES_Response__1up CategoryReduction_JET_EffectiveNP_Detector1__1down CategoryReduction_JET_EffectiveNP_Detector1__1up CategoryReduction_JET_EffectiveNP_Detector2__1down CategoryReduction_JET_EffectiveNP_Detector2__1up CategoryReduction_JET_EffectiveNP_Mixed1__1down CategoryReduction_JET_EffectiveNP_Mixed1__1up CategoryReduction_JET_EffectiveNP_Mixed2__1down CategoryReduction_JET_EffectiveNP_Mixed2__1up CategoryReduction_JET_EffectiveNP_Mixed3__1down CategoryReduction_JET_EffectiveNP_Mixed3__1up CategoryReduction_JET_EffectiveNP_Modelling1__1down CategoryReduction_JET_EffectiveNP_Modelling1__1up CategoryReduction_JET_EffectiveNP_Modelling2__1down CategoryReduction_JET_EffectiveNP_Modelling2__1up CategoryReduction_JET_EffectiveNP_Modelling3__1down CategoryReduction_JET_EffectiveNP_Modelling3__1up CategoryReduction_JET_EffectiveNP_Modelling4__1down CategoryReduction_JET_EffectiveNP_Modelling4__1up CategoryReduction_JET_EffectiveNP_Statistical1__1down CategoryReduction_JET_EffectiveNP_Statistical1__1up CategoryReduction_JET_EffectiveNP_Statistical2__1down CategoryReduction_JET_EffectiveNP_Statistical2__1up CategoryReduction_JET_EffectiveNP_Statistical3__1down CategoryReduction_JET_EffectiveNP_Statistical3__1up CategoryReduction_JET_EffectiveNP_Statistical4__1down CategoryReduction_JET_EffectiveNP_Statistical4__1up CategoryReduction_JET_EffectiveNP_Statistical5__1down CategoryReduction_JET_EffectiveNP_Statistical5__1up CategoryReduction_JET_EffectiveNP_Statistical6__1down CategoryReduction_JET_EffectiveNP_Statistical6__1up CategoryReduction_JET_EtaIntercalibration_Modelling__1down CategoryReduction_JET_EtaIntercalibration_Modelling__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up CategoryReduction_JET_EtaIntercalibration_TotalStat__1down CategoryReduction_JET_EtaIntercalibration_TotalStat__1up CategoryReduction_JET_Flavor_Composition__1down CategoryReduction_JET_Flavor_Composition__1up CategoryReduction_JET_Flavor_Response__1down CategoryReduction_JET_Flavor_Response__1up CategoryReduction_JET_Pileup_OffsetMu__1down CategoryReduction_JET_Pileup_OffsetMu__1up CategoryReduction_JET_Pileup_OffsetNPV__1down CategoryReduction_JET_Pileup_OffsetNPV__1up CategoryReduction_JET_Pileup_PtTerm__1down CategoryReduction_JET_Pileup_PtTerm__1up CategoryReduction_JET_Pileup_RhoTopology__1down CategoryReduction_JET_Pileup_RhoTopology__1up CategoryReduction_JET_PunchThrough_MC16__1down CategoryReduction_JET_PunchThrough_MC16__1up CategoryReduction_JET_SingleParticle_HighPt__1down CategoryReduction_JET_SingleParticle_HighPt__1up CategoryReduction_JET_JER_DataVsMC_MC16__1down CategoryReduction_JET_JER_DataVsMC_MC16__1up CategoryReduction_JET_JER_EffectiveNP_1__1down CategoryReduction_JET_JER_EffectiveNP_1__1up CategoryReduction_JET_JER_EffectiveNP_2__1down CategoryReduction_JET_JER_EffectiveNP_2__1up CategoryReduction_JET_JER_EffectiveNP_3__1down CategoryReduction_JET_JER_EffectiveNP_3__1up CategoryReduction_JET_JER_EffectiveNP_4__1down CategoryReduction_JET_JER_EffectiveNP_4__1up CategoryReduction_JET_JER_EffectiveNP_5__1down CategoryReduction_JET_JER_EffectiveNP_5__1up CategoryReduction_JET_JER_EffectiveNP_6__1down CategoryReduction_JET_JER_EffectiveNP_6__1up CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up EG_RESOLUTION_ALL__1down EG_RESOLUTION_ALL__1up EG_SCALE_AF2__1down EG_SCALE_AF2__1up EG_SCALE_ALL__1down EG_SCALE_ALL__1up MET_SoftTrk_ResoPara MET_SoftTrk_ResoPerp MET_SoftTrk_ScaleDown MET_SoftTrk_ScaleUp MUON_ID__1down MUON_ID__1up MUON_MS__1down MUON_MS__1up MUON_SAGITTA_RESBIAS__1down MUON_SAGITTA_RESBIAS__1up MUON_SAGITTA_RHO__1down MUON_SAGITTA_RHO__1up MUON_SCALE__1down MUON_SCALE__1up"


## systematics that are stored as weights in files and correspond to lepton, jvt and pileup
systematicsWEIGHT="jvt_DOWN   jvt_UP   pileup_DOWN   pileup_UP   leptonSF_EL_SF_ID_DOWN   leptonSF_EL_SF_ID_UP   leptonSF_EL_SF_Isol_DOWN   leptonSF_EL_SF_Isol_UP   leptonSF_EL_SF_Reco_DOWN   leptonSF_EL_SF_Reco_UP   trigger_EL_SF_Trigger_DOWN   trigger_EL_SF_Trigger_UP   leptonSF_MU_SF_ID_STAT_DOWN   leptonSF_MU_SF_ID_STAT_LOWPT_DOWN   leptonSF_MU_SF_ID_STAT_LOWPT_UP   leptonSF_MU_SF_ID_STAT_UP   leptonSF_MU_SF_ID_SYST_DOWN   leptonSF_MU_SF_ID_SYST_LOWPT_DOWN   leptonSF_MU_SF_ID_SYST_LOWPT_UP   leptonSF_MU_SF_ID_SYST_UP   leptonSF_MU_SF_Isol_STAT_DOWN   leptonSF_MU_SF_Isol_STAT_UP   leptonSF_MU_SF_Isol_SYST_DOWN   leptonSF_MU_SF_Isol_SYST_UP   leptonSF_MU_SF_TTVA_STAT_DOWN   leptonSF_MU_SF_TTVA_STAT_UP   leptonSF_MU_SF_TTVA_SYST_DOWN   leptonSF_MU_SF_TTVA_SYST_UP   trigger_MU_SF_STAT_DOWN   trigger_MU_SF_STAT_UP   trigger_MU_SF_SYST_DOWN   trigger_MU_SF_SYST_UP"


## systematics that are stored as tree in files
systematicsTREE="JET_BJES_Response__1down
   JET_BJES_Response__1up
   JET_EffectiveNP_Detector1__1down
   JET_EffectiveNP_Detector1__1up
   JET_EffectiveNP_Detector2__1down
   JET_EffectiveNP_Detector2__1up
   JET_EffectiveNP_Mixed1__1down
   JET_EffectiveNP_Mixed1__1up
   JET_EffectiveNP_Mixed2__1down
   JET_EffectiveNP_Mixed2__1up
   JET_EffectiveNP_Mixed3__1down
   JET_EffectiveNP_Mixed3__1up
   JET_EffectiveNP_Modelling1__1down
   JET_EffectiveNP_Modelling1__1up
   JET_EffectiveNP_Modelling2__1down
   JET_EffectiveNP_Modelling2__1up
   JET_EffectiveNP_Modelling3__1down
   JET_EffectiveNP_Modelling3__1up
   JET_EffectiveNP_Modelling4__1down
   JET_EffectiveNP_Modelling4__1up
   JET_EffectiveNP_Statistical1__1down
   JET_EffectiveNP_Statistical1__1up
   JET_EffectiveNP_Statistical2__1down
   JET_EffectiveNP_Statistical2__1up
   JET_EffectiveNP_Statistical3__1down
   JET_EffectiveNP_Statistical3__1up
   JET_EffectiveNP_Statistical4__1down
   JET_EffectiveNP_Statistical4__1up
   JET_EffectiveNP_Statistical5__1down
   JET_EffectiveNP_Statistical5__1up
   JET_EffectiveNP_Statistical6__1down
   JET_EffectiveNP_Statistical6__1up
   JET_EtaIntercalibration_Modelling__1down
   JET_EtaIntercalibration_Modelling__1up
   JET_EtaIntercalibration_NonClosure_2018data__1down
   JET_EtaIntercalibration_NonClosure_2018data__1up
   JET_EtaIntercalibration_NonClosure_highE__1down
   JET_EtaIntercalibration_NonClosure_highE__1up
   JET_EtaIntercalibration_NonClosure_negEta__1down
   JET_EtaIntercalibration_NonClosure_negEta__1up
   JET_EtaIntercalibration_NonClosure_posEta__1down
   JET_EtaIntercalibration_NonClosure_posEta__1up
   JET_EtaIntercalibration_TotalStat__1down
   JET_EtaIntercalibration_TotalStat__1up
   JET_Flavor_Composition__1down
   JET_Flavor_Composition__1up
   JET_Flavor_Response__1down
   JET_Flavor_Response__1up
   JET_Pileup_OffsetMu__1down
   JET_Pileup_OffsetMu__1up
   JET_Pileup_OffsetNPV__1down
   JET_Pileup_OffsetNPV__1up
   JET_Pileup_PtTerm__1down
   JET_Pileup_PtTerm__1up
   JET_Pileup_RhoTopology__1down
   JET_Pileup_RhoTopology__1up
   JET_PunchThrough_MC16__1down
   JET_PunchThrough_MC16__1up
   JET_SingleParticle_HighPt__1down
   JET_SingleParticle_HighPt__1up
   JET_JER_DataVsMC_MC16__1down
   JET_JER_DataVsMC_MC16__1down_PseudoData
   JET_JER_DataVsMC_MC16__1up
   JET_JER_DataVsMC_MC16__1up_PseudoData
   JET_JER_EffectiveNP_1__1down
   JET_JER_EffectiveNP_1__1up
   JET_JER_EffectiveNP_2__1down
   JET_JER_EffectiveNP_2__1up
   JET_JER_EffectiveNP_3__1down
   JET_JER_EffectiveNP_3__1up
   JET_JER_EffectiveNP_4__1down
   JET_JER_EffectiveNP_4__1up
   JET_JER_EffectiveNP_5__1down
   JET_JER_EffectiveNP_5__1up
   JET_JER_EffectiveNP_6__1down
   JET_JER_EffectiveNP_6__1up
   JET_JER_EffectiveNP_7__1down
   JET_JER_EffectiveNP_7__1up
   JET_JER_EffectiveNP_8__1down
   JET_JER_EffectiveNP_8__1up
   JET_JER_EffectiveNP_9__1down
   JET_JER_EffectiveNP_9__1up
   JET_JER_EffectiveNP_10__1down
   JET_JER_EffectiveNP_10__1up
   JET_JER_EffectiveNP_11__1down
   JET_JER_EffectiveNP_11__1up
   JET_JER_EffectiveNP_12restTerm__1down
   JET_JER_EffectiveNP_12restTerm__1up
   JET_JER_EffectiveNP_1__1down_PseudoData
   JET_JER_EffectiveNP_1__1up_PseudoData
   JET_JER_EffectiveNP_2__1down_PseudoData
   JET_JER_EffectiveNP_2__1up_PseudoData
   JET_JER_EffectiveNP_3__1down_PseudoData
   JET_JER_EffectiveNP_3__1up_PseudoData
   JET_JER_EffectiveNP_4__1down_PseudoData
   JET_JER_EffectiveNP_4__1up_PseudoData
   JET_JER_EffectiveNP_5__1down_PseudoData
   JET_JER_EffectiveNP_5__1up_PseudoData
   JET_JER_EffectiveNP_6__1down_PseudoData
   JET_JER_EffectiveNP_6__1up_PseudoData
   JET_JER_EffectiveNP_7__1down_PseudoData
   JET_JER_EffectiveNP_7__1up_PseudoData
   JET_JER_EffectiveNP_8__1down_PseudoData
   JET_JER_EffectiveNP_8__1up_PseudoData
   JET_JER_EffectiveNP_9__1down_PseudoData
   JET_JER_EffectiveNP_9__1up_PseudoData
   JET_JER_EffectiveNP_10__1down_PseudoData
   JET_JER_EffectiveNP_10__1up_PseudoData
   JET_JER_EffectiveNP_11__1down_PseudoData
   JET_JER_EffectiveNP_11__1up_PseudoData
   JET_JER_EffectiveNP_12restTerm__1down_PseudoData
   JET_JER_EffectiveNP_12restTerm__1up_PseudoData
   EG_RESOLUTION_ALL__1down
   EG_RESOLUTION_ALL__1up
   EG_SCALE_AF2__1down
   EG_SCALE_AF2__1up
   EG_SCALE_ALL__1down
   EG_SCALE_ALL__1up
   MET_SoftTrk_ResoPara
   MET_SoftTrk_ResoPerp
   MET_SoftTrk_ScaleDown
   MET_SoftTrk_ScaleUp
   MUON_CB__1down
   MUON_CB__1up
   MUON_SAGITTA_DATASTAT__1down
   MUON_SAGITTA_DATASTAT__1up
   MUON_SAGITTA_RESBIAS__1down
   MUON_SAGITTA_RESBIAS__1up
   MUON_SCALE__1down
   MUON_SCALE__1up"

#systematics_ttbar_stop="$systematicsSCALES $systematicsBTAG $systematicsWEIGHT" #4
systematics_ttbar_stop="$systematicsWEIGHT" #4
#systematics_diboson=" $systematicsBTAG $systematicsWEIGHT"   # 40
systematics_diboson="$systematicsWEIGHT"   # 40
#systematics_wzjets=" $systematicsSCALES_wzjets $systematicsBTAG $systematicsWEIGHT"   # 40
systematics_wzjets="$systematicsWEIGHT"   # 40

IFS='/' read -ra ADDR <<< "$dirpath"

#echo "${ADDR[-1]}"

if [ ${ADDR[-1]} == "mc.Wenu.txt" ] || [ ${ADDR[-1]} == "mc.Wmunu.txt" ] || [ ${ADDR[-1]} == "mc.Wtaunu.txt" ]; then
    samples="mc.Wenu.txt mc.Wmunu.txt mc.Wtaunu.txt"
    if [ "$syst_type" == "weights" ]; then
        systematics_one=$systematics_wzjets
    else
        systematics_one=$systematicsTREE
    fi

elif [ ${ADDR[-1]} == "mc.Zee.txt" ] || [ ${ADDR[-1]} == "mc.Zmumu.txt" ] || [ ${ADDR[-1]} == "mc.Ztautau.txt" ]; then 
    samples="mc.Zee.txt mc.Zmumu.txt mc.Ztautau.txt"
    if [ "$syst_type" == "weights" ]; then
        systematics_one=$systematics_wzjets
    else
        systematics_one=$systematicsTREE
    fi
else
    samples=${ADDR[-1]}
    if [ "$syst_type" == "weights" ]; then
        systematics_one=$systematics_wzjets
    else
        systematics_one=$systematicsTREE
    fi
fi


###################################
today=$(date +'%d_%m_%Y')

#echo "Samples: $samples"

if [ "$syst_type" == "weights" ]; then
    prefix="w"
else
    prefix="t"
fi

for sample in $samples
do
  # execute 1
    log=${prefix}_info_${today}_${sample}
    if [ -f syst_sub_folder/${log} ]; then
        rm syst_sub_folder/${log}
    fi
    for syst in $systematics_one
    do
        #echo -e "INFO\t Submitting: $syst"
        #cmd="cd $here; ./SingleLepSelection.py -i filelists/$sample.txt -p $name -s $syst "
        if [ "$channel" == "1L" ] 
        then
            #path=/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/SingleLepSelection/filelists_all_merged
            path=$(echo "${dirpath/${ADDR[-1]}/$sample}")
            cmd="./SingleLepSelection.py -i ${path} -p $name -s $syst "
        else
            #path=/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/DileptonSelection/filelists_ttbar_p5057/
            #cmd="./DileptonSelection.py -i ${path}/$sample.txt -p $name -s $syst "
            path=$(echo "${dirpath/${ADDR[-1]}/$sample}")
            cmd="./DileptonSelection.py -i $path -p $name -s $syst "
        fi
        # submission happens here
    echo "${syst},${cmd}" >> syst_sub_folder/${log} #_submission_${syst_type}_${sample_name[1]}.txt
  done
  echo -e "INFO\t File syst_sub_folder/${log} written successfully."
  #cat submission_dummy.txt
  csub -n $log -j syst_sub_folder/${log}
  #rm submission_dummy.txt
done

