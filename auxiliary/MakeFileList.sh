#!/bin/bash

Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-p|-o|-h]"
   echo "options:"
   echo "-i     Path to the folder containing the datasets, e.g. /eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/singlelepton/ntuples_ATR22_Apr2022_mc20d/mc/Wjets/"
   echo "-o     Path to the folder containing the filelists; default is /gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/filelists/"
   echo "-h     Print this Help."
   echo
}


#RESET THE LAST OPTION PROCESSED
OPTIND=1


while getopts o:i:h flag
do
    case "${flag}" in
        o) outdir=${OPTARG};;
        i) dirpath=${OPTARG};;
		h) Help
		   return 1;;
		:)  echo 'missing mandatory argument!' >&2
            return 1;;
		\?) echo 'invalid option!' >&2
            return 1;;
		
    esac
done

if [ -z $dirpath ]; then
    echo "ERROR: missing mandatory argument! Run ./MakeFileList.sh -h for help."
    return 1
fi 

if [ -z $outdir ]; then
    outdir=/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/prova/
fi 

if [ ! -d $outdir ]; then
    mkdir -p $outdir
fi

rm_data="rm ${outdir}data.txt"
trigger=0

for dir in ${dirpath}*; do
    IFS='.' read -ra ADDR <<< "$dir"
    dsid=${ADDR[2]}
    
    if [ -z $dsid ]; then
        continue
    fi 


    echo $dsid
    case "${dsid}" in
        601348) outfile="mc.stop.schan.601348.txt";;
        
        601349) outfile="mc.stop.schan.601349.txt";;

        601350) outfile="mc.stop.tchan.601350.txt";;

        601351) outfile="mc.stop.tchan.601351.txt";;

        601352) outfile="mc.stop.WtDR.1l.601352.txt";;
        
        601353) outfile="mc.stop.WtDR.2l.601353.txt";;

        601354) outfile="mc.stop.WtDR.2l.601354.txt";;

        601355) outfile="mc.stop.WtDR.1l.601355.txt";;    

#------------------------------------------------------------------
        
        700566) outfile="mc.diboson.700566.txt";;

        700567) outfile="mc.diboson.700567.txt";;
        
        700568) outfile="mc.diboson.700568.txt";;

        700569) outfile="mc.diboson.700569.txt";;

        700570) outfile="mc.diboson.700570.txt";;

        700571) outfile="mc.diboson.700571.txt";;

        700572) outfile="mc.diboson.700572.txt";;

        700573) outfile="mc.diboson.700573.txt";;

        700574) outfile="mc.diboson.700574.txt";;

        700575) outfile="mc.diboson.700575.txt";;

        700600) outfile="mc.diboson.700600.txt";;

        700601) outfile="mc.diboson.700601.txt";;

        700602) outfile="mc.diboson.700602.txt";;

        700603) outfile="mc.diboson.700603.txt";;

        700604) outfile="mc.diboson.700604.txt";;

        700605) outfile="mc.diboson.700605.txt";;


#------------------------------------------------------------------

        601229) outfile="mc.ttbar.nominal.txt";; #SINGLELEP

        601230) outfile="mc.ttbar.nominal.txt";; #DILEP

        601399) outfile="mc.ttbar.nominal.hdamp.txt";; #HDAMP

        601415) outfile="mc.ttbar.nominal.herwig.txt";; #Herwig

#------------------------------------------------------------------

        # 601237) outfile="mc.ttbar.nominal.dilep.txt";;

        601189) outfile="mc.Zjets.p8.601189.txt";;

        601190) outfile="mc.Zjets.p8.601190.txt";;

        601191) outfile="mc.Zjets.p8.601191.txt";;

        700615) outfile="mc.Zjets.700615.txt";;

        700616) outfile="mc.Zjets.700616.txt";;

        700617) outfile="mc.Zjets.700617.txt";;

        700618) outfile="mc.Zjets.700618.txt";;

        700619) outfile="mc.Zjets.700619.txt";;

        700620) outfile="mc.Zjets.700620.txt";;
        
        700621) outfile="mc.Zjets.700621.txt";;

        700622) outfile="mc.Zjets.700622.txt";;

        700623) outfile="mc.Zjets.700623.txt";;

        700624) outfile="mc.Zjets.700624.txt";;

        700625) outfile="mc.Zjets.700625.txt";;

        700626) outfile="mc.Zjets.700626.txt";;

        700627) outfile="mc.Zjets.700627.txt";;

#------------------------------------------------------------------

        700606) outfile="mc.Wjets.700606.txt";;

        700607) outfile="mc.Wjets.700607.txt";;

        700608) outfile="mc.Wjets.700608.txt";;

        700609) outfile="mc.Wjets.700609.txt";;

        700610) outfile="mc.Wjets.700610.txt";;

        700611) outfile="mc.Wjets.700611.txt";;

        700612) outfile="mc.Wjets.700612.txt";;

        700613) outfile="mc.Wjets.700613.txt";;

        700614) outfile="mc.Wjets.700614.txt";;

        # 411233) outfile="mc.ttbar_alt.411233.txt";;

        # 411234) outfile="mc.ttbar_alt.411234.txt";;

        # 412116) outfile="mc.ttbar_alt.412116.txt";;

        # 412117) outfile="mc.ttbar_alt.412117.txt";;

        *)      outfile="data.txt"
                if [ -f "${outdir}data.txt" ]; then #rm data file only on the first iteration
                    eval $rm_data
                    rm_data=""
                fi
                trigger=1;;
        
        #*)  echo "missing this DSID: ${dsid}"
        #    continue ;;
    esac
    
    if [ $trigger=='1' ]; then #for data we append the lines to the file
        find "$(cd $dir; pwd)" -mindepth 1 -type f -name "user*.root" >> ${outdir}${outfile}
    else
        find "$(cd $dir; pwd)" -mindepth 1 -type f -name "user*.root" > ${outdir}${outfile}
    fi
    trigger=0
done

