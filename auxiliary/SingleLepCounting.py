#!/usr/bin/env python
import os, sys, time
import argparse
sys.path.append('../')

timer_start = time.time()

from array import array
from numpy import median

import ROOT as rt

from common import *
import helper_functions

########################################################
parser = argparse.ArgumentParser(description='ttbar 13.6 TeV: Single Lepton selection maker')
parser.add_argument( '-i', '--filelistname', default="filelists/mc.ttbar.nominal_short.txt" )
parser.add_argument( '-s', '--systematic',   default="nominal" )
parser.add_argument( '-p', '--preselection', default="check" )
parser.add_argument( '-o', '--outfilename',  default="" )

args         = parser.parse_args()
filelistname = args.filelistname
syst         = args.systematic
preselection = args.preselection
outfilename  = args.outfilename

# check what is data and what is not
isData = False
if filelistname.find('data') > -1: isData = True

isQCD = False
if filelistname.find('qcd') > -1:
    isData = True
    isQCD = True

isWjets = False
if filelistname.find('Wjets') > -1: isWjets = True



# check I am reading a filelist
if filelistname == "":
    print "ERROR: please specify a file list"
    exit(1)

# default name of the tree
treename = "nominal"

## here begins the systs change, in case it is not nominal
if not isData:
    # search in common.py for all the systs defined in systematics_tree
    if syst in systematics_tree:
        treename = syst # change the name of the tree to be read
    else:
        treename = "nominal"

if isQCD:
    treename = "nominal_Loose"

print "INFO: running filelistname: ", filelistname
print "INFO: running preselection: ", preselection
print "INFO: running systematic  : ", syst
print "INFO: reading TTree                               : ", treename

################################################################################

# Get ROOT trees
tree = rt.TChain( treename, treename )
for fname in open( filelistname, 'r' ).readlines():
    fname = fname.strip()
    tree.Add( fname )

ext = filelistname.split('/')[-1].split('.')[-1]

print "INFO:", filelistname.split('/')[-1].replace( ext, "%s.root"%syst)
# Event loop
nentries = tree.GetEntries()
print "INFO: looping over %i entries" % nentries

