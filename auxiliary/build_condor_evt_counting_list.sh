#!/bin/bash

find "$(cd .; pwd)" -type f -name "mc*.txt" -printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' | awk '{ print "/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/auxiliary/SingleLepCounting.py -i ", $0 }' > condor_counting_list.txt
find "$(cd .; pwd)" -type f -name "data*.txt" -printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' | awk '{ print "/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/auxiliary/SingleLepCounting.py -i ", $0 }' >> condor_counting_list.txt
