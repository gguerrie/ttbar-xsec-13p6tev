#!/bin/bash
Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-p|-o|-h]"
   echo "options:"
   echo "-i     Path to the folder containing the datasets, e.g. /gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/filelists/"
   echo "-o     Output folder name (equals the option -p in the python script) "
   echo "-c     Name of the channel: 1L or 2L"
   echo "-h     Print this Help."
   echo
}

#RESET THE LAST OPTION PROCESSED
OPTIND=1

while getopts o:c:i:h flag
do
    case "${flag}" in
        o) outdir=${OPTARG};;
        i) dirpath=${OPTARG};;
        c) channel=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            exit 1;;
		\?) echo 'invalid option!' >&2
            exit 1;;
		
    esac
done

here=`pwd`

if [ -z $dirpath ]; then
    echo "ERROR: missing mandatory argument! Run -h option for help."
    exit 1
fi 

if [ -z $outdir ]; then
    outdir=check
fi 

if [ ! -d $outdir ]; then
    mkdir -p $here/output/$outdir
fi

echo -e "INFO\t Cleaning up previous resubmission.."
if [ -f $here/condor_process_list.txt ]
then
    rm -rf $here/condor_process_list.txt
fi

path=$here/$dirpath

for filename in `ls -t ${path}/*.txt`; do

    #echo $filename
    #separate path to obtain the filename, and then remove the path
    IFS='/' read -ra ADDR <<< $filename ; IFS='.' read -ra name <<< ${ADDR[-1]}

    #echo "${name[0]}"

     if [ "${name[0]}" == "data" ]; then
        if [ "$channel" == "2L" ]
        then 
            echo "${name[0]},/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/DileptonSelection/DileptonSelection.py -p $outdir -i ${filename}" >> $here/condor_process_list.txt
        else
            echo "${name[0]},/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/SingleLeptonSelection/SingleLepSelection.py -p $outdir -i ${filename}" >> $here/condor_process_list.txt
        fi
     fi
     if [ "${name[0]}" == "mc" ]; then 
        mc_name=${name[@]/${name[0]}}; mc_name=${mc_name[@]/${name[-1]}}
        mc_name=( "${mc_name[@]// /_}" )
        suffix=$(printf '%s\n' "${mc_name[@]}")
        if [ "$channel" == "2L" ]
        then 
            echo "$suffix,/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/DileptonSelection/DileptonSelection.py -p $outdir -i ${filename}" >> $here/condor_process_list.txt
        else
            echo "$suffix,/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/SingleLeptonSelection/SingleLepSelection.py -p $outdir -i ${filename}" >> $here/condor_process_list.txt
        fi
     fi

done