#!/bin/bash
name=$1

max=$(ls -dq ${name}*.out | wc -l)
max=$(expr $max - 1)

if [ -f "counting_summary.txt" ]; then
    rm counting_summary.txt
fi

for i in $(seq 0 $max); do

    file=${name}_$i.out
    tail -2 $file >> counting_summary.txt

done
