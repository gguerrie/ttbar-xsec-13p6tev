#!/usr/bin/env python
import ROOT  as  rt
import sys
import os.path
from lxml import etree as ET
import argparse

parser = argparse.ArgumentParser(description='ttbar 13.6 TeV: SumW creator')
parser.add_argument( '-i', '--filelistdir', default="/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/DileptonSelection/filelists_not_merged" )
parser.add_argument( '-o', '--outfilename', default="xml/SumW.all.xml" )

args         = parser.parse_args()
RootPath = args.filelistdir
xmlfname = args.outfilename


SystNameConvDict_stop = {
    "MUR1_MUF2_PDF260000": "Scale_muF_UP",
    "MUR1_MUF0.5_PDF260000": "Scale_muF_DOWN",
    "MUR2_MUF1_PDF260000": "Scale_muR_UP",
    "MUR0.5_MUF1_PDF260000": "Scale_muR_DOWN",
    "Var3cUp": "Var3c_UP",
    "Var3cDown": "Var3c_DOWN",
    "isr:muRfac=1.0_fsr:muRfac=2.0": "FSR_UP",
    "isr:muRfac=1.0_fsr:muRfac=0.5": "FSR_DOWN",
    }

SystNameConvDict_stop_tchan = {
    "MUR1_MUF2_PDF260400": "Scale_muF_UP",
    "MUR1_MUF0.5_PDF260400": "Scale_muF_DOWN",
    "MUR2_MUF1_PDF260400": "Scale_muR_UP",
    "MUR0.5_MUF1_PDF260400": "Scale_muR_DOWN",
    "Var3cUp": "Var3c_UP",
    "Var3cDown": "Var3c_DOWN",
    "isr:muRfac=1.0_fsr:muRfac=2.0": "FSR_UP",
    "isr:muRfac=1.0_fsr:muRfac=0.5": "FSR_DOWN",
    }

SystNameConvDict_stop_list = [SystNameConvDict_stop, SystNameConvDict_stop_tchan, SystNameConvDict_stop]

SystNameConvDict_Zjets = {
    "MUR0.5_MUF0.5_PDF303200_PSMUR0.5_PSMUF0.5" : "muR0.5_muF0.5",
    "MUR0.5_MUF1_PDF303200_PSMUR0.5_PSMUF1"     : "muR0.5_muF1.0",
    "MUR1_MUF0.5_PDF303200_PSMUR1_PSMUF0.5"     : "muR1.0_muF0.5",
    "MUR1_MUF1_PDF303200"                       : "muR1.0_muF1.0",
    "MUR1_MUF2_PDF303200_PSMUR1_PSMUF2"         : "muR1.0_muF2.0",
    "MUR2_MUF1_PDF303200_PSMUR2_PSMUF1"         : "muR2.0_muF1.0",
    "MUR2_MUF2_PDF303200_PSMUR2_PSMUF2"         : "muR2.0_muF2.0",
    }

SystNameConvDict_Wjets = {
    "MUR0.5_MUF0.5_PDF303200_PSMUR0.5_PSMUF0.5" : "muR0.5_muF0.5",
    "MUR0.5_MUF1_PDF303200_PSMUR0.5_PSMUF1"     : "muR0.5_muF1.0",
    "MUR1_MUF0.5_PDF303200_PSMUR1_PSMUF0.5"     : "muR1.0_muF0.5",
    "MUR1_MUF1_PDF303200"                       : "muR1.0_muF1.0",
    "MUR1_MUF2_PDF303200_PSMUR1_PSMUF2"         : "muR1.0_muF2.0",
    "MUR2_MUF1_PDF303200_PSMUR2_PSMUF1"         : "muR2.0_muF1.0",
    "MUR2_MUF2_PDF303200_PSMUR2_PSMUF2"         : "muR2.0_muF2.0",
}

SystNameConvDict_ttbar = {
    "MUR1_MUF2_PDF260000": "Scale_muF_UP",
    "MUR1_MUF0.5_PDF260000": "Scale_muF_DOWN",
    "MUR2_MUF1_PDF260000": "Scale_muR_UP",
    "MUR0.5_MUF1_PDF260000": "Scale_muR_DOWN",
    "Var3cUp": "Var3c_UP",
    "Var3cDown": "Var3c_DOWN",
    "isr:muRfac=1.0_fsr:muRfac=2.0": "FSR_UP",
    "isr:muRfac=1.0_fsr:muRfac=0.5": "FSR_DOWN",
    }

for idx, i in enumerate(range(93300,93343)):
    name = 'MUR1_MUF1_PDF' + str(i)
    idx+=1
    value = 'ttbar_PDF_' + str(idx)
    SystNameConvDict_ttbar.update({name:value})


#####################################################################################################################################################################
######################################################### Create indexlists for samples #############################################################################
#####################################################################################################################################################################

#To check these, open  up a grid output, do sumWeights->Scan("*", "", "colsize=60") and check wether the index below corresponds to the encoded syst. 
#IndexList1 = [1, 2, 3, 4, 166, 167, 171, 172] #Stop - schan and tchan, all ISR unc.
#IndexList2 = [1, 2, 3, 4, 142, 143, 147, 148] #Stop - WtDR, all ISR unc.

# Valid for ttbar AND stop
IndexList_ttbar = [2, 3, 4, 5, 167, 168, 172, 173]
IndexList_stop_schan =  [6, 5, 3, 2, 165, 166, 170, 171]
IndexList_stop_tchan =  [6, 5, 3, 2, 163, 164, 168, 169]
IndexList_stop = [IndexList_stop_schan, IndexList_stop_tchan, IndexList_stop_schan]

# Add PDFs to ttbar
for i in range(124,167):
    IndexList_ttbar.append(i)

IndexList_Zjets = [5,7,9,11,13,15,17]
IndexList_Wjets = [5,7,9,11,13,15,17]


#####################################################################################################################################################################
#####################################################################################################################################################################
#####################################################################################################################################################################


def GetTchain(filename):
    tree = rt.TChain( "sumWeights", "sumWeights" )
    if os.path.exists(filename):
        for file in open(filename).readlines():
            file = file.strip()
            tree.Add( file )
        return tree
    else:
        return

def CreateDSIDxml(f, fIsTree=True, TotalEventsWeighted=False):
    if fIsTree ==  False: tree = f.sumWeights
    else: tree = f
    nentries = tree.GetEntries()
    NomSumW = 0
    for evt in range(nentries):
        tree.GetEntry(evt)
        dsid = str(tree.dsid)
        if tree.dsid in range(700615,700627): # Sherpa z-jet samples
            print("The DSID is: " + dsid)
            if TotalEventsWeighted == False: NomSumW += tree.totalEventsWeighted_mc_generator_weights[0] # Default tree
            else:  
                print("INFO:   ",dsid, tree.totalEventsWeighted)
                NomSumW +=  tree.totalEventsWeighted
        else:
            if TotalEventsWeighted == False: NomSumW += tree.totalEventsWeighted_mc_generator_weights[1] # Nominal tree
            else:  
                print("INFO:   ",dsid, tree.totalEventsWeighted)
                NomSumW +=  tree.totalEventsWeighted
    root = ET.Element("MCInfo", DSID=dsid, NominalSumW="%s" % NomSumW)
    return root

def CreateSystxml(f, idxList, SystNameConvDict, fIsTree=True):
    if fIsTree ==  False: tree = f.sumWeights
    else: tree = f
    nentries = tree.GetEntries()
    NomSumW = 0
    for evt in range(nentries):
        tree.GetEntry(evt)
        dsid = str(tree.dsid)
        if tree.dsid in range(700615,700627): # Sherpa z-jet samples
            NomSumW += tree.totalEventsWeighted_mc_generator_weights[0] # Default tree
        else: 
            NomSumW += tree.totalEventsWeighted_mc_generator_weights[1] # Nominal tree


    root = ET.Element("MCInfo", DSID=dsid, NominalSumW="%s" % NomSumW)
    for i in idxList:
        SumW = 0
        for evt in range(nentries):
            tree.GetEntry(evt)
            SumW += tree.totalEventsWeighted_mc_generator_weights[i]
            SumWName = tree.names_mc_generator_weights[i].strip()
        print("INFO:   " + str(i) + ", " + SumWName + ", " + "sumW: " + str(round(SumW, 2)) + ", " + "NomSumW: " + str(NomSumW))
        systName = ET.SubElement(root, "Systematic", GenSystName=SumWName, GeneratorIdx=str(i), GeneratorWeight=str(SumW), CorrFactor=str(NomSumW/SumW), OurSystName=SystNameConvDict[SumWName])
    return root


#RootPath = "/afs/cern.ch/user/s/sasingh/development/ttbar_xs_5tev/Giulio/filelists_lxplus"
f_dict = {
    # s-channel
    '601348': GetTchain("%s/mc.stop.schan.601348.txt" % RootPath),
    
    '601349': GetTchain("%s/mc.stop.schan.601349.txt" % RootPath),
    # t-channel
    '601350': GetTchain("%s/mc.stop.tchan.601350.txt" % RootPath),

    '601351': GetTchain("%s/mc.stop.tchan.601351.txt" % RootPath),
    # tW inclusive
    '601352': GetTchain("%s/mc.stop.WtDR.1l.601352.txt" % RootPath),

    '601355': GetTchain("%s/mc.stop.WtDR.1l.601355.txt" % RootPath),
    # tW dilepton
    '601353': GetTchain("%s/mc.stop.WtDR.2l.601353.txt" % RootPath),

    '601354': GetTchain("%s/mc.stop.WtDR.2l.601354.txt" % RootPath),

    

#------------------------------------------------------------------
    # "Semileptonic" VV sherpa 2.2.12 - not really semileptonic...
    '700566': GetTchain("%s/mc.diboson.700566.txt" % RootPath),

    '700567': GetTchain("%s/mc.diboson.700567.txt" % RootPath),
    
    '700568': GetTchain("%s/mc.diboson.700568.txt" % RootPath),

    '700569': GetTchain("%s/mc.diboson.700569.txt" % RootPath),

    '700570': GetTchain("%s/mc.diboson.700570.txt" % RootPath),

    '700571': GetTchain("%s/mc.diboson.700571.txt" % RootPath),

    '700572': GetTchain("%s/mc.diboson.700572.txt" % RootPath),

    '700573': GetTchain("%s/mc.diboson.700573.txt" % RootPath),

    '700574': GetTchain("%s/mc.diboson.700574.txt" % RootPath),

    '700575': GetTchain("%s/mc.diboson.700575.txt" % RootPath),
    # Multilepton+neutrino VV sherpa 2.2.12
    '700600': GetTchain("%s/mc.diboson.700600.txt" % RootPath),

    '700601': GetTchain("%s/mc.diboson.700601.txt" % RootPath),

    '700602': GetTchain("%s/mc.diboson.700602.txt" % RootPath),

    '700603': GetTchain("%s/mc.diboson.700603.txt" % RootPath),

    '700604': GetTchain("%s/mc.diboson.700604.txt" % RootPath),

    '700605': GetTchain("%s/mc.diboson.700605.txt" % RootPath),


#------------------------------------------------------------------
    # ttbar
    '601229': GetTchain("%s/mc.ttbar.nominal.txt" % RootPath), #SINGLELEP

    '601230': GetTchain("%s/mc.ttbar.nominal.txt" % RootPath), #DILEP

    # ttbar alt

    '601399': GetTchain("%s/mc.ttbar.nominal.hdamp.txt" % RootPath), #HDAMP

    '601415': GetTchain("%s/mc.ttbar.nominal.herwig.txt" % RootPath), #Herwig

#------------------------------------------------------------------

    #Zjets pythia
    '601189': GetTchain("%s/mc.Zjets.p8.601189.txt" % RootPath),

    '601190': GetTchain("%s/mc.Zjets.p8.601190.txt" % RootPath),

    '601191': GetTchain("%s/mc.Zjets.p8.601191.txt" % RootPath),
    # Z(ee)+jets sherpa 2.2.12
    '700615': GetTchain("%s/mc.Zjets.700615.txt" % RootPath),

    '700616': GetTchain("%s/mc.Zjets.700616.txt" % RootPath),

    '700617': GetTchain("%s/mc.Zjets.700617.txt" % RootPath),
    # Z(mumu)+jets sherpa 2.2.12
    '700618': GetTchain("%s/mc.Zjets.700618.txt" % RootPath),

    '700619': GetTchain("%s/mc.Zjets.700619.txt" % RootPath),

    '700620': GetTchain("%s/mc.Zjets.700620.txt" % RootPath),
    # Z(tautau)+jets sherpa 2.2.12
    '700621': GetTchain("%s/mc.Zjets.700621.txt" % RootPath),

    '700622': GetTchain("%s/mc.Zjets.700622.txt" % RootPath),

    '700623': GetTchain("%s/mc.Zjets.700623.txt" % RootPath),
    # Z(nunu)+jets sherpa 2.2.12
    '700624': GetTchain("%s/mc.Zjets.700624.txt" % RootPath),

    '700625': GetTchain("%s/mc.Zjets.700625.txt" % RootPath),

    '700626': GetTchain("%s/mc.Zjets.700626.txt" % RootPath),

    '700627': GetTchain("%s/mc.Zjets.700627.txt" % RootPath),

#------------------------------------------------------------------
    # W(->e nu)+jets sherpa 2.2.12 
    '700606': GetTchain("%s/mc.Wjets.700606.txt" % RootPath),

    '700607': GetTchain("%s/mc.Wjets.700607.txt" % RootPath),

    '700608': GetTchain("%s/mc.Wjets.700608.txt" % RootPath),
    # W(->mu nu)+jets sherpa 2.2.12
    '700609': GetTchain("%s/mc.Wjets.700609.txt" % RootPath),

    '700610': GetTchain("%s/mc.Wjets.700610.txt" % RootPath),

    '700611': GetTchain("%s/mc.Wjets.700611.txt" % RootPath),
    # W(->tau nu)+jets sherpa 2.2.12
    '700612': GetTchain("%s/mc.Wjets.700612.txt" % RootPath),

    '700613': GetTchain("%s/mc.Wjets.700613.txt" % RootPath),

    '700614': GetTchain("%s/mc.Wjets.700614.txt" % RootPath),

}


def ttbar_altXML():
    XMLList = []
    XMLList += [CreateDSIDxml(f_dict['601399'], TotalEventsWeighted=True)]
    XMLList += [CreateDSIDxml(f_dict['601415'], TotalEventsWeighted=True)]
    return XMLList


def StopXML():
    DSIDs_schan = ['601348',
                   '601349']

    DSIDs_tchan = ['601350',
                   '601351',]

    DSIDs_tW = ['601352',
                '601355',
                '601353',
                '601354']
    DSIDs = [DSIDs_schan, DSIDs_tchan, DSIDs_tW]
    XMLList = []

    for idx, DSID in enumerate(DSIDs):
        ConvDict_stop = SystNameConvDict_stop_list[idx]
        indexlist = IndexList_stop[idx]
        for entry in DSID:
            try:
                XMLList += [CreateSystxml(f_dict[entry], indexlist, ConvDict_stop)]
            except:
                print("WARNING:    Skipping " + entry)
                pass
    return XMLList

def ttbarXML():
    XMLList = []
    XMLList += [CreateSystxml(f_dict['601230'], IndexList_ttbar, SystNameConvDict_ttbar, fIsTree=True)]
    # try:
    #     XMLList += [CreateSystxml(f_dict['410472'], IndexList_ttbar, SystNameConvDict_ttbar, fIsTree=True)]
    # except:
    #     pass
    return XMLList


def ZjetsXML():
    DSIDs = ['601189',
            '601190',
            '601191',
            '700615',
            '700616',
            '700617',
            '700618',
            '700619',
            '700620',
            '700621',
            '700622',
            '700623',
            '700624',
            '700625',
            '700626',
            '700627']
    XMLList = []

    for entry in DSIDs:
        try:
            XMLList += [CreateSystxml(f_dict[entry], IndexList_Zjets, SystNameConvDict_Zjets, fIsTree=True)]
        except:
            print("WARNING:    Skipping " + entry)
            pass
    return XMLList

def WjetsXML():
    DSIDs = ['700606',
            '700607',
            '700608',
            '700609',
            '700610',
            '700611',
            '700612',
            '700613',
            '700614']
    XMLList = []

    for entry in DSIDs:
        try:
            XMLList += [CreateSystxml(f_dict[entry], IndexList_Wjets, SystNameConvDict_Wjets, fIsTree=True)]
        except:
            print("WARNING:    Skipping " + entry)
            pass

    return XMLList

def DibosonXML():
    
    DSIDs = ['700566',
            '700567',
            '700568',
            '700569',
            '700570',
            '700571',
            '700572',
            '700573',
            '700574',
            '700575',
            '700600',
            '700601',
            '700602',
            '700603',
            '700604',
            '700605']
    XMLList = []

    for entry in DSIDs:
        try:
            XMLList += [CreateDSIDxml(f_dict[entry], TotalEventsWeighted=True)]
        except:
            print("WARNING:    Skipping " + entry)
            pass

    return XMLList

XMLList = []

#try:
XMLList += ttbarXML()
#except:
#    print("WARNING:    Skipping ttbar")
#    pass # doing nothing on exception
try:
    XMLList += ttbar_altXML() # block raising an exception
except:
    print("WARNING:    Skipping ttbar ALT")
    pass # doing nothing on exception

try:
    XMLList += StopXML() # block raising an exception
except:
    print("WARNING:    Skipping stop")
    pass # doing nothing on exception

try:
    XMLList += ZjetsXML() # block raising an exception
except:
    print("WARNING:    Skipping zjets")
    pass # doing nothing on exception

try:
    XMLList += WjetsXML() # block raising an exception
except:
    print("WARNING:    Skipping wjets")
    pass # doing nothing on exception

try:
    XMLList += DibosonXML() # block raising an exception
except:
    print("WARNING:    Skipping diboson")
    pass # doing nothing on exception


root = ET.Element("Info")
for element in XMLList: root.append(element)
#xmlfname = 'xml/SumW.all.short.xml'
et = ET.ElementTree(root)
et.write(xmlfname, pretty_print=True)
print("INFO:    Everything went fine.")
#with open(xmlfname, "w") as fh:
#   fh.write(ET.tostring(root, pretty_print=True))

