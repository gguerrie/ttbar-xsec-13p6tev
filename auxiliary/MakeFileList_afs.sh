#!/bin/bash

Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-p|-o|-h]"
   echo "options:"
   echo "-p     Path to the folder containing the datasets, e.g. /eos/infnts/atlas/gguerrie/13p6_TeV/ntuples/singlelepton/ntuples_ATR22_Apr2022_mc20d/mc/Wjets/"
   echo "-o     Path to the folder containing the filelists; default is /gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/filelists/"
   echo "-h     Print this Help."
   echo
}


#RESET THE LAST OPTION PROCESSED
OPTIND=1


while getopts o:p:h flag
do
    case "${flag}" in
        o) outdir=${OPTARG};;
        p) dirpath=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            exit 1;;
		\?) echo 'invalid option!' >&2
            exit 1;;
		
    esac
done

if [ -z $dirpath ]; then
    echo "ERROR: missing mandatory argument! Run ./MakeFileList.sh -h for help."
    exit 1
fi 

if [ -z $outdir ]; then
    outdir=/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/prova/
fi 

if [ ! -d $outdir ]; then
    mkdir -p $outdir
fi

rm_data="rm ${outdir}data.txt"
trigger=0

for dir in ${dirpath}*; do
    IFS='/' read -ra ADDR <<< "$dir"
    dsid=${ADDR[-1]}
    
    if [ -z $dsid ]; then
        continue
    fi 


    echo $dsid
    case "${dsid}" in
        410644) outfile="mc.stop.schan.410644.txt";;
        
        410645) outfile="mc.stop.schan.410645.txt";;
        
        410646) outfile="mc.stop.WtDR.410646.txt";;

        410647) outfile="mc.stop.WtDR.410647.txt";;   

        410658) outfile="mc.stop.tchan.410658.txt";;

        410659) outfile="mc.stop.tchan.410659.txt";;
        
        363356) outfile="mc.diboson.363356.txt";;

        363358) outfile="mc.diboson.363358.txt";;
        
        363359) outfile="mc.diboson.363359.txt";;

        363360) outfile="mc.diboson.363360.txt";;

        363489) outfile="mc.diboson.363489.txt";;

        364250) outfile="mc.diboson.364250.txt";;

        364253) outfile="mc.diboson.364253.txt";;

        364254) outfile="mc.diboson.364254.txt";;

        364255) outfile="mc.diboson.364255.txt";;

        410470) outfile="mc.ttbar.nominal.txt";; #Does not have mc_generator_names....will have to modify this script if we want to use it

        700320) outfile="mc.Zjets.700320.txt";;
        
        700321) outfile="mc.Zjets.700321.txt";;

        700322) outfile="mc.Zjets.700322.txt";;

        700323) outfile="mc.Zjets.700323.txt";;

        700324) outfile="mc.Zjets.700324.txt";;

        700325) outfile="mc.Zjets.700325.txt";;

        700326) outfile="mc.Zjets.700326.txt";;

        700327) outfile="mc.Zjets.700327.txt";;

        700328) outfile="mc.Zjets.700328.txt";;

        700329) outfile="mc.Zjets.700329.txt";;

        700330) outfile="mc.Zjets.700330.txt";;

        700331) outfile="mc.Zjets.700331.txt";;

        700332) outfile="mc.Zjets.700332.txt";;

        700333) outfile="mc.Zjets.700333.txt";;

        700334) outfile="mc.Zjets.700334.txt";;

        700338) outfile="mc.Wjets.700338.txt";;

        700339) outfile="mc.Wjets.700339.txt";;

        700340) outfile="mc.Wjets.700340.txt";;

        700341) outfile="mc.Wjets.700341.txt";;

        700342) outfile="mc.Wjets.700342.txt";;

        700343) outfile="mc.Wjets.700343.txt";;

        700344) outfile="mc.Wjets.700344.txt";;

        700345) outfile="mc.Wjets.700345.txt";;

        700346) outfile="mc.Wjets.700346.txt";;

        700347) outfile="mc.Wjets.700347.txt";;

        700348) outfile="mc.Wjets.700348.txt";;

        700349) outfile="mc.Wjets.700349.txt";;

        411233) outfile="mc.ttbar_alt.411233.txt";;

        411234) outfile="mc.ttbar_alt.411234.txt";;

        412116) outfile="mc.ttbar_alt.412116.txt";;

        412117) outfile="mc.ttbar_alt.412117.txt";;

        *)      outfile="data.txt"
                if [ -f "${outdir}data.txt" ]; then #rm data file only on the first iteration
                    eval $rm_data
                    rm_data=""
                fi
                trigger=1;;
        
        #*)  echo "missing this DSID: ${dsid}"
        #    continue ;;
    esac
    
    if [ $trigger=='1' ]; then #for data we append the lines to the file
        find "$(cd $dir; pwd)" -mindepth 1 -name "user*.root" >> ${outdir}${outfile}
    else
        find "$(cd $dir; pwd)" -mindepth 1 -name "user*.root" > ${outdir}${outfile}
    fi
    trigger=0
done

