#!/bin/bash

Help()
{
   # Display Help
   echo
   echo "Syntax: ./MakeFileList.sh [-p|-o|-h]"
   echo "options:"
   echo "-i     Path to the folder containing the datasets,"
   echo "-o     Path to the folder containing the filelists; default is /gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/filelists/"
   echo "-h     Print this Help."
   echo
}

#RESET THE LAST OPTION PROCESSED
OPTIND=1

while getopts o:i:h flag
do
    case "${flag}" in
        o) outdir=${OPTARG};;
        i) dirpath=${OPTARG};;
		h) Help
		   exit;;
		:)  echo 'missing mandatory argument!' >&2
            exit 1;;
		\?) echo 'invalid option!' >&2
            exit 1;;
		
    esac
done

if [ -z $dirpath ]; then
    echo "ERROR: missing mandatory argument! Run -h for help."
    exit 1
fi 

if [ -z $outdir ]; then
    outdir=/gpfs/atlas/gguerrie/ttbarxsec_run3/ttbar-xsec-13p6tev/prova/
fi 

if [ ! -d $outdir ]; then
    mkdir -p $outdir
fi

here=`pwd`

path=$here/$dirpath
#path=$dirpath

cat $path/*diboson*.txt > $outdir/mc.diboson.txt

cat $path/mc.Zjets.700615.txt $path/mc.Zjets.700616.txt $path/mc.Zjets.700617.txt > $outdir/mc.Zee.txt
cat $path/mc.Zjets.700618.txt $path/mc.Zjets.700619.txt $path/mc.Zjets.700620.txt > $outdir/mc.Zmumu.txt
cat $path/mc.Zjets.700621.txt $path/mc.Zjets.700622.txt $path/mc.Zjets.700623.txt $path/mc.Zjets.700624.txt $path/mc.Zjets.700625.txt $path/mc.Zjets.700626.txt $path/mc.Zjets.700575.txt $path/mc.Zjets.700576.txt $path/mc.Zjets.700577.txt > $outdir/mc.Ztautau.txt

cat $path/mc.Wjets.700606.txt $path/mc.Wjets.700607.txt $path/mc.Wjets.700608.txt > $outdir/mc.Wenu.txt
cat $path/mc.Wjets.700609.txt $path/mc.Wjets.700610.txt $path/mc.Wjets.700611.txt > $outdir/mc.Wmunu.txt
cat $path/mc.Wjets.700612.txt $path/mc.Wjets.700613.txt $path/mc.Wjets.700614.txt > $outdir/mc.Wtaunu.txt
cat $path/mc.ttbar_alt* > $outdir/mc.ttbar_alt.txt

cat $path/mc.stop.schan.601348.txt $path/mc.stop.schan.601349.txt > $outdir/mc.stop.schan.txt
cat $path/mc.stop.tchan.601350.txt $path/mc.stop.tchan.601351.txt > $outdir/mc.stop.tchan.txt
cat $path/mc.stop.WtDR.2l.601353.txt $path/mc.stop.WtDR.2l.601354.txt > $outdir/mc.stop.WtDR.2l.txt
cat $path/mc.stop.WtDR.1l.601352.txt $path/mc.stop.WtDR.1l.601355.txt > $outdir/mc.stop.WtDR.1l.txt


cp ${path}mc.ttbar*.txt $outdir/
cp ${path}data*.txt $outdir/

#rm mc.Wjets.*.txt
#rm mc.Zjets.*.txt
#m mc.diboson.*.txt