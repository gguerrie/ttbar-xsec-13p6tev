## copy of helper_functions_1312_NOMUONCRACK 

import numpy as np
from common import *
from ROOT import *
from math import cos, sin, sqrt, pow
from lxml import etree as ET
import os

#####################
Dir = os.path.dirname(os.path.abspath(__file__))
#xmlFname = Dir + "/xml/SumW.all.short.xml"
#xmlFname = Dir + "/xml/boh_ma_funziona.xml"
xmlFname = Dir + "/xml/SumW.all.test.xml" # Dummy value for CI pipeline
XMLtree = ET.parse(xmlFname) #Hard coded, can be updated w/ CreateXml.py
def ReadXmlTree( syst, dsid ):
	try:  
		CorrFactor = float(XMLtree.find("./MCInfo[@DSID='%s']/Systematic[@OurSystName='%s']" % (dsid, syst)).attrib['CorrFactor']) #Grab correction factor for correct SumW renormalization
		Idx = int(XMLtree.find("./MCInfo[@DSID='%s']/Systematic[@OurSystName='%s']" % (dsid, syst)).attrib['GeneratorIdx']) #Index in mc_generator_weights corresponding to systematic
		return CorrFactor, Idx
	except AttributeError: return False, False

#####################

def GetEventWeight( lep, tree, syst, b_tag_cut, dsid):
    CorrectionFactor, Index = ReadXmlTree(syst, dsid)
    w_extra = 1
    #print("Correction factor: ", CorrectionFactor, "Idx: ", Index)

    if Index != False:
        wIdx = tree.mc_generator_weights[Index]
        #print "Index: ", Index
        #print "mc_generator_weights[Index]: ", wIdx
        #print "mc_generator_weights[0]: ", tree.mc_generator_weights[0]

        if wIdx == 0:
            print "Exceptional WARNING!!! the value of index %s is: " %  (Index), wIdx
            print "DEBUG: Event number = %-10i run number = %-10i " % ( tree.eventNumber, tree.runNumber )
        w_extra = CorrectionFactor * wIdx / tree.mc_generator_weights[0]
        #print "w_extra: ", w_extra


    w = 1.
    NomSumW = float(XMLtree.find("./MCInfo[@DSID='%s']" % dsid).attrib['NominalSumW'])
    #print "Nominal Sum of weights: ", NomSumW

    # Rel.22 let's multiply it by the lepton, pileup, JVT and btagging weights already here
    w_leptonSF = tree.weight_leptonSF
    #w_trigger  = tree.weight_trigger
    w_trigger = 1.
    w_beamspot   = tree.weight_beamspot
    w_jvt      = tree.weight_jvt
    w_pileup  =   tree.weight_pileup

    if   b_tag_cut==60 : w_btag = tree.weight_bTagSF_DL1dv01_60 
    elif b_tag_cut==70 : w_btag = tree.weight_bTagSF_DL1dv01_70
    elif b_tag_cut==77 : w_btag = tree.weight_bTagSF_DL1dv01_77
    elif b_tag_cut==85 : w_btag = tree.weight_bTagSF_DL1dv01_85
    else:
       print(">>>> ERROR: please specify a b_tag_cut!!!!!!")
  
  

    
    if syst == "nominal":
        pass
    
    elif syst in [ "pileup_UP", "pileup_DOWN" ]:
        w_pileup = tree.weight_pileup_UP if syst == "pileup_UP" else tree.weight_pileup_DOWN # change w_pileup by the UP/DOWN
        #print "CHECK: in syst=pileup_UP, w_pileup: ", w_pileup

    elif syst in [ "jvt_UP", "jvt_DOWN" ]:
        w_jvt = tree.weight_jvt_UP if syst == "jvt_UP" else tree.weight_jvt_DOWN # change w_jvt by the UP/DOWN variations
        #print "CHECK: in syst=jvt_UP, w_jvt: ", w_jvt
        
    elif syst.startswith("bTagSF"):
        if "eigenvars" in syst:
            k = int( syst.split('_')[-1] )
            syst_btag   = syst.replace( "_up_%i"%k, "_up" ).replace( "_down_%i"%k, "_down" )
            syst_branch = "weight_%s" % syst_btag
            #print "CHECK: in syst=btagSF eigenvars, syst_branch: ", syst_branch
            exec( "w_btag = tree.%s[%i]" % (syst_branch, k ) )   # change w_btag by the UP/DOWN variations
            #print "CHECK: in syst=btagSF, w_btag: ", w_btag

            
        else:
            syst_branch = "weight_%s" % syst
            #print "CHECK: in syst=btagSF extrapolation, syst_branch: ", syst_branch	
            exec( "w_btag = tree.%s" % syst_branch )
            #print "CHECK: in syst=btagSF, w_btag: ", w_btag

            
    elif syst.startswith("leptonSF"):
        syst_branch = "weight_%s" % syst
        #print "CHECK: in syst=leptonSF, syst_branch: ", syst_branch	
        exec( "w_leptonSF = tree.%s" % syst_branch )
        #print "CHECK: in syst=leptonSF, w_leptonSF: ", w_leptonSF

    #ADDING NEW STUFF
    elif syst.startswith("trigger"):
        syst_branch = "weight_%s" % syst
        #print "CHECK: in syst=leptonSF, syst_branch: ", syst_branch	
        exec( "w_trigger = tree.%s" % syst_branch )
        #print "CHECK: in syst=leptonSF, w_leptonSF: ", w_leptonSF


    # print("---------------------------")
    # print "w_leptonSF: ", w_leptonSF
    # print "w_trigger: ", w_trigger
    # print "w_beamspot: ", w_beamspot
    # print "w_jvt: ", w_jvt
    # print "w_btag: ", w_btag
    # print "w_pileup: ", w_pileup
    # print "NomSumW: ", NomSumW
    

    w *= w_leptonSF * w_trigger * w_beamspot* w_jvt * w_btag * w_extra * w_pileup
    #print "w: ", w
    #print "w_extra: ", w_extra
    w /= NomSumW    
    return w, w_extra


#####################

def MakeEventElectron( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0], tree.el_e[0] )
    electron_mass =  0.51099895  # in MEV
    lep.SetPtEtaPhiM( tree.el_pt[0], tree.el_cl_eta[0], tree.el_phi[0],  electron_mass)
    lep.q = tree.el_charge[0]
    lep.flav = 11 * lep.q
  #  lep.topoetcone = tree.el_topoetcone20[0]
  #  lep.ptvarcone  = tree.el_ptvarcone20[0]
  #  lep.d0sig      = tree.el_d0sig[0]
  #  lep.ITrk       = tree.el_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.el_pt)
    lep.Crack      = False
    if 1.37 < abs(lep.Eta()) < 1.52: lep.Crack = True
    return lep

#~~~~~~~~~~~~

def MakeEventMuon( tree ):
    lep = TLorentzVector()
#    lep.SetPtEtaPhiE( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], tree.mu_e[0] )
    muon_mass = 105.6583755 # in MeV   
    lep.SetPtEtaPhiM( tree.mu_pt[0], tree.mu_eta[0], tree.mu_phi[0], muon_mass )
    lep.q = tree.mu_charge[0]
    lep.flav = 13 * lep.q
 #   lep.topoetcone = tree.mu_topoetcone20[0]
 #   lep.ptvarcone  = tree.mu_ptvarcone30[0]
 #   lep.d0sig      = tree.mu_d0sig[0]
 #   lep.ITrk       = tree.mu_ptvarcone40[0]/lep.Pt()
    lep.N          = len(tree.mu_pt)
    lep.Crack      = False
    return lep

#~~~~~~~~~~~~~~~~~~
#### set cut on btagging DL1r value for 77% WP and the corresponding weigth is bTagSF_DL1r_77
def MakeEventJets( tree, jet_pt_cut, b_tag_cut):
    jets = []
    bjets = []

    #print "INFO: jet pT cut set to %f " % jet_pt_cut
    #print "INFO: jet WP cut set to %i " % b_tag_cut


    # this is in case we want to limit the number of jets
    max=999

    # in case is needed, we use up to the four highest pT jets
    jets_n = len( tree.jet_pt )
    #print "INFO: number of jets as length of jet_pt vector is: ", jets_n

    if jets_n > max : jets_n = max
    #print "INFO: number of jets used in MakeEventJets is: ", jets_n
       
    for i in range(jets_n):


        jet_pt = 0.

        # mass issue
        jet_before = TLorentzVector()
        jet_mass = 0.
        jet_before.SetPtEtaPhiE( tree.jet_pt[i], tree.jet_eta[i], tree.jet_phi[i], tree.jet_e[i])
        jet_mass = jet_before.M()

#        print "INFO: jet iterator i is: ", i
#        print "INFO: jet eta: ", tree.jet_eta[i]
#        print "INFO: jet pT: ", tree.jet_pt[i]/GeV
#        print "INFO: DL1r 60 is: ", float(ord(tree.jet_isbtagged_DL1r_60[i]))
#        print "INFO: 70 is: ",      float(ord(tree.jet_isbtagged_DL1r_70[i]))
#        print "INFO: 77 is: ",      float(ord(tree.jet_isbtagged_DL1r_77[i]))


        #  default value 
        jet_pt = tree.jet_pt[i]
        jet_eta = tree.jet_eta[i]

        # make a cut on jets of jet_pt_cut [in GeV]
        if jet_pt/GeV < jet_pt_cut: continue
       
        jets += [ TLorentzVector() ]

        ## now with mass instead of energy
        jets[-1].SetPtEtaPhiM( jet_pt, tree.jet_eta[i], tree.jet_phi[i], jet_mass )
       
        jets[-1].index = i

        # Tomas decided not to store this info
        # jets[-1].dl1 = tree.jet_DL1r[i]

        # general definition
        jets[-1].isBtagged = False

        # per WP definition
        #jets[-1].isbtagged_DL1r_85 = float(ord(tree.jet_isbtagged_DL1r_85[i]))
        jets[-1].isbtagged_DL1dv01_77 = float(ord(tree.jet_isbtagged_DL1dv01_77[i]))
        #jets[-1].isbtagged_DL1r_70 = float(ord(tree.jet_isbtagged_DL1r_70[i]))
        #jets[-1].isbtagged_DL1r_60 = float(ord(tree.jet_isbtagged_DL1r_60[i]))

        # Rel. 22 : make the cut on the value
        if jets[-1].isbtagged_DL1dv01_77 == 1 : 
            jets[-1].isBtagged = True
            bjets += [ TLorentzVector(jets[-1]) ]
            bjets[-1].index = i
            #print "INFO: bjet identified : ", bjets[-1].index



    return jets, bjets

#~~~~~~~~~~~~

def CalcMinDPhi_lj( lep, jets ):
   min_dPhi = 10.
   jets_n = len(jets)
   for i in range(jets_n):
     j = jets[i]
     dPhi = lep.DeltaPhi(j)
     if abs(dPhi) > abs(min_dPhi): continue

     min_dPhi = dPhi

   return min_dPhi

#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~ 
## MVA variables
#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~#~~~~~~~~~~~~

def EventShape( all_fso ):
   D = 0.
           
   for p in all_fso:
     D += p.P()*p.P()

   S = TMatrixD(3,3)
   for p in all_fso:
     p3 = [ p.Px(), p.Py(), p.Pz() ]

     for i in range(3):
       for j in range(3):
         S[i][j] += p3[i] * p3[j] / D

   S_eigen = TMatrixDEigen(S)
   S_evals = S_eigen.GetEigenValuesRe()
   l1 = S_evals[0]
   l2 = S_evals[1]
   l3 = S_evals[2]

   aplanarity   = 1.5 * l3
   variable_D   = 27.* (l1 * l2 * l3)

   return aplanarity, variable_D


def EventShapeVariables( all_fso ):

  fSumE = 0.
  FW_2 = 0.
  fH_2 = 0. 
           
  for p in all_fso:
    fSumE += p.E()
    for d in all_fso:
	x = cos(p.Angle(d.Vect()));
	fH_2 += abs(p.P())*abs(d.P())*(0.5*(3*x*x-1))

 
  FW_2 = fH_2 / (fSumE*fSumE)

  return FW_2

#~~~~~~~~~~~~
# lep is TLVector
def CalcLeptonBJetVariables( lep, bjets ):
    pairs_dR = []  # dR_lj
    pairs_dEta = [] # dEta_lb

    bjets_n = len(bjets)
    for i in range(bjets_n):
        b = bjets[i]
        lb = lep + b
        pairs_dR       += [ lep.DeltaR(b) ]
        pairs_dEta     += [ abs( lep.Eta() - b.Eta() ) ]
                
    pairs_dR.sort()        # here the MIN value is at 0
    pairs_dEta.sort()      # here the MIN value is at 0
    
    return pairs_dR, pairs_dEta


#~~~~~~~~~~~~~
# find as system of a b-tagged jet and two other jets with the maximum pT out of all possible three-jet permutations
def CalcTripleJet( jets, bjets ):

    pairs_dR = []

    max_pt = 0.
    dR12 = 0.
    dR13 = 0.
    dR23 = 0.
        
    jets_n = len(jets)
    for i in range(jets_n):
        for k in range( i+1, jets_n ):
            for bj in bjets:

                j1 = jets[i]
                j2 = jets[k]
                if j1.index == j2.index: continue

                sum = 0.
                sum = j1.isBtagged + j2.isBtagged 
                if sum > 0 : continue

                if bj.index == j1.index: continue
                if bj.index == j2.index: continue

                jjb = j1 + j2 + bj

                pT_jjb = jjb.Pt()

                # dR of highest pT trijet system
                if pT_jjb > max_pt: 
                   max_pt = pT_jjb

                   dR12 = j1.DeltaR(j2)
                   dR13 = j1.DeltaR(bj)
                   dR23 = j2.DeltaR(bj)
             
                pairs_dR += [ dR12 ]
                pairs_dR += [ dR13 ]
                pairs_dR += [ dR23 ]

 
    pairs_dR.sort(reverse=True) 
                
    return pairs_dR
 


#~~~~~~~~~~~~~
# two jets
def CalcJetJetVariables( jets ):

    all_pairs_mass = []
    pairs_dR = []  # dR_jj
    pairs_dEta = []  # dEta_jj 

    min_dR = 1000.
    mjj_mindR = 0.

    jets_n = len(jets)
    for i in range(jets_n):
        for k in range( i+1, jets_n ):
            j1 = jets[i]
            j2 = jets[k]
            if j1.index == j2.index: continue  # just check again the jets are not the same

            jj = j1 + j2

            all_pairs_mass += [ jj.M() ]

            dR = j1.DeltaR(j2)    
            pairs_dR       += [ dR ]

            dEta_jj        = abs( j1.Eta() - j2.Eta() )
            pairs_dEta     += [ dEta_jj ]

            # mjj min dR
            if dR < min_dR: 
               min_dR = dR
               mjj_mindR = jj.M() # mass in this permutation

    all_pairs_mass.sort()            # here the minimal value is at 0
    pairs_dR.sort()     # here the minimal value is at 0
    pairs_dEta.sort(reverse=True)    # here the maximal value is at 0
    
    return all_pairs_mass, pairs_dR, pairs_dEta, mjj_mindR


#~~~~~~~~~~~~~
# two untagged jets
def CalcUJetUJetVariables( jets ):

    all_pairs_mass = []
    pairs_dR = []  # dR_jj
    pairs_dEta = []  # dEta_jj 

    min_dR = 1000.
    mjj_mindR = 0.

    jets_n = len(jets)
    for i in range(jets_n):
        for k in range( i+1, jets_n ):
            j1 = jets[i]
            j2 = jets[k]
            if j1.index == j2.index: continue  # just check again the jets are not the same

            # we need only untagged jets!
            if j1.isBtagged == True : continue
            if j2.isBtagged == True : continue

            jj = j1 + j2

            all_pairs_mass += [ jj.M() ]

            dR = j1.DeltaR(j2)    
            pairs_dR       += [ dR ]

            dEta_jj        = abs( j1.Eta() - j2.Eta() )
            pairs_dEta     += [ dEta_jj ]

            # mjj min dR
            if dR < min_dR: 
               min_dR = dR
               mjj_mindR = jj.M() # mass in this permutation

    all_pairs_mass.sort()            # here the minimal value is at 0
    pairs_dR.sort()     # here the minimal value is at 0
    pairs_dEta.sort(reverse=True)    # here the maximal value is at 0
    
    return all_pairs_mass, pairs_dR, pairs_dEta, mjj_mindR


#~~~~~~~~~~~~~
# build HT2p, excluding the leading jet up to the five leading jets and deviding by the sum of pz components of all objects (no neutrino)
def CalcHT2p( lep, jets ):

   jets_n = len(jets)

   # initial values
   nom = 0.    # start with the 2nd jet excluding the leading jet 
   value_pz = 0.  # all jets pZ + lepton

   if jets_n >= 2: 
     nom += jets[1].Pt()
   if jets_n >= 3: 
     nom += jets[2].Pt()
   if jets_n >= 4: 
     nom += jets[3].Pt()
   if jets_n >= 5: 
     nom += jets[4].Pt()

   value_pz  += abs( lep.Pz() )

   if jets_n >= 2: 
     value_pz  +=  abs(jets[0].Pz()) + abs(jets[1].Pz()) 
   if jets_n >= 3: 
     value_pz  +=  abs(jets[2].Pz())
   if jets_n >= 4: 
     value_pz  +=  abs(jets[3].Pz())
   if jets_n >= 5: 
     value_pz  +=  abs(jets[4].Pz())

   HT2p = nom / value_pz

   return HT2p


#############################################################################################################################
