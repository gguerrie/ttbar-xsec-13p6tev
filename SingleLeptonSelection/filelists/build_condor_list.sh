#!/bin/bash

find "$(cd .; pwd)" -type f -name "mc*.txt" -printf '%h\0%d\0%p\n' | sort -t '\0' -n | awk -F '\0' '{print $3}' > condor_list.txt
