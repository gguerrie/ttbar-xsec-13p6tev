//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Apr  1 15:03:52 2022 by ROOT version 6.18/04
// from TTree nominal/tree
// found on file: /home/lserkin/eos/13p6TeV/ntuples/Tomas/user.tdado.410470.PhPy8EG.DAOD_PHYS.e6337_s3681_r13145_p4926.TT_1L_March_3_v1_output_root/user.tdado.28320969._000008.output.root
//////////////////////////////////////////////////////////

#ifndef _h
#define _h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"

class  {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   vector<float>   *mc_generator_weights;
   Float_t         weight_mc;
   Float_t         weight_pileup;
   Float_t         weight_beamspot;
   Float_t         weight_leptonSF;
   Float_t         weight_trigger;
   Float_t         weight_bTagSF_DL1r_Continuous;
   Float_t         weight_bTagSF_DL1r_60;
   Float_t         weight_bTagSF_DL1r_70;
   Float_t         weight_bTagSF_DL1r_77;
   Float_t         weight_bTagSF_DL1r_85;
   Float_t         weight_jvt;
   Float_t         weight_pileup_UP;
   Float_t         weight_pileup_DOWN;
   Float_t         weight_leptonSF_EL_SF_Reco_UP;
   Float_t         weight_leptonSF_EL_SF_Reco_DOWN;
   Float_t         weight_leptonSF_EL_SF_ID_UP;
   Float_t         weight_leptonSF_EL_SF_ID_DOWN;
   Float_t         weight_leptonSF_EL_SF_Isol_UP;
   Float_t         weight_leptonSF_EL_SF_Isol_DOWN;
   Float_t         weight_trigger_EL_SF_Trigger_UP;
   Float_t         weight_trigger_EL_SF_Trigger_DOWN;
   Float_t         weight_trigger_MU_SF_STAT_UP;
   Float_t         weight_trigger_MU_SF_STAT_DOWN;
   Float_t         weight_trigger_MU_SF_SYST_UP;
   Float_t         weight_trigger_MU_SF_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;
   Float_t         weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_Isol_SYST_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_STAT_DOWN;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_UP;
   Float_t         weight_leptonSF_MU_SF_TTVA_SYST_DOWN;
   Float_t         weight_jvt_UP;
   Float_t         weight_jvt_DOWN;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_60_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_60_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_70_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_70_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_77_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_77_extrapolation_from_charm_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_B_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_C_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_Light_up;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_B_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_C_down;
   vector<float>   *weight_bTagSF_DL1r_85_eigenvars_Light_down;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_up;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_down;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_from_charm_up;
   Float_t         weight_bTagSF_DL1r_85_extrapolation_from_charm_down;
   ULong64_t       eventNumber;
   UInt_t          runNumber;
   UInt_t          randomRunNumber;
   UInt_t          mcChannelNumber;
   Float_t         mu;
   Float_t         mu_actual;
   UInt_t          backgroundFlags;
   vector<float>   *el_pt;
   vector<float>   *el_eta;
   vector<float>   *el_cl_eta;
   vector<float>   *el_phi;
   vector<float>   *el_e;
   vector<float>   *el_charge;
   vector<char>    *el_CF;
   vector<float>   *mu_pt;
   vector<float>   *mu_eta;
   vector<float>   *mu_phi;
   vector<float>   *mu_e;
   vector<float>   *mu_charge;
   vector<float>   *jet_pt;
   vector<float>   *jet_eta;
   vector<float>   *jet_phi;
   vector<float>   *jet_e;
   vector<float>   *jet_jvt;
   vector<int>     *jet_truthPartonLabel;
   vector<int>     *jet_tagWeightBin_DL1r_Continuous;
   vector<char>    *jet_isbtagged_DL1r_60;
   vector<char>    *jet_isbtagged_DL1r_70;
   vector<char>    *jet_isbtagged_DL1r_77;
   vector<char>    *jet_isbtagged_DL1r_85;
   Float_t         met_met;
   Float_t         met_sumet;
   Float_t         met_phi;
   Int_t           ejets_2015;
   Int_t           ejets_2016;
   Int_t           ejets_2017;
   Int_t           ejets_2018;
   Int_t           mujets_2015;
   Int_t           mujets_2016;
   Int_t           mujets_2017;
   Int_t           mujets_2018;
   Int_t           hadBjetIndex;
   Int_t           lightJetIndex1;
   Int_t           lightJetIndex2;
   Float_t         chi2;

   // List of branches
   TBranch        *b_mc_generator_weights;   //!
   TBranch        *b_weight_mc;   //!
   TBranch        *b_weight_pileup;   //!
   TBranch        *b_weight_beamspot;   //!
   TBranch        *b_weight_leptonSF;   //!
   TBranch        *b_weight_trigger;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous;   //!
   TBranch        *b_weight_bTagSF_DL1r_60;   //!
   TBranch        *b_weight_bTagSF_DL1r_70;   //!
   TBranch        *b_weight_bTagSF_DL1r_77;   //!
   TBranch        *b_weight_bTagSF_DL1r_85;   //!
   TBranch        *b_weight_jvt;   //!
   TBranch        *b_weight_pileup_UP;   //!
   TBranch        *b_weight_pileup_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Reco_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_ID_DOWN;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_UP;   //!
   TBranch        *b_weight_leptonSF_EL_SF_Isol_DOWN;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_UP;   //!
   TBranch        *b_weight_trigger_EL_SF_Trigger_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_STAT_DOWN;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_UP;   //!
   TBranch        *b_weight_trigger_MU_SF_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_Isol_SYST_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_UP;   //!
   TBranch        *b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN;   //!
   TBranch        *b_weight_jvt_UP;   //!
   TBranch        *b_weight_jvt_DOWN;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_60_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_B_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_C_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_Light_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_B_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_C_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_eigenvars_Light_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_down;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_from_charm_up;   //!
   TBranch        *b_weight_bTagSF_DL1r_85_extrapolation_from_charm_down;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_randomRunNumber;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_mu;   //!
   TBranch        *b_mu_actual;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_cl_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_e;   //!
   TBranch        *b_el_charge;   //!
   TBranch        *b_el_CF;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_e;   //!
   TBranch        *b_mu_charge;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_e;   //!
   TBranch        *b_jet_jvt;   //!
   TBranch        *b_jet_truthPartonLabel;   //!
   TBranch        *b_jet_tagWeightBin_DL1r_Continuous;   //!
   TBranch        *b_jet_isbtagged_DL1r_60;   //!
   TBranch        *b_jet_isbtagged_DL1r_70;   //!
   TBranch        *b_jet_isbtagged_DL1r_77;   //!
   TBranch        *b_jet_isbtagged_DL1r_85;   //!
   TBranch        *b_met_met;   //!
   TBranch        *b_met_sumet;   //!
   TBranch        *b_met_phi;   //!
   TBranch        *b_ejets_2015;   //!
   TBranch        *b_ejets_2016;   //!
   TBranch        *b_ejets_2017;   //!
   TBranch        *b_ejets_2018;   //!
   TBranch        *b_mujets_2015;   //!
   TBranch        *b_mujets_2016;   //!
   TBranch        *b_mujets_2017;   //!
   TBranch        *b_mujets_2018;   //!
   TBranch        *b_hadBjetIndex;   //!
   TBranch        *b_lightJetIndex1;   //!
   TBranch        *b_lightJetIndex2;   //!
   TBranch        *b_chi2;   //!

   (TTree *tree=0);
   virtual ~();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef _cxx
::(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/home/lserkin/eos/13p6TeV/ntuples/Tomas/user.tdado.410470.PhPy8EG.DAOD_PHYS.e6337_s3681_r13145_p4926.TT_1L_March_3_v1_output_root/user.tdado.28320969._000008.output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/home/lserkin/eos/13p6TeV/ntuples/Tomas/user.tdado.410470.PhPy8EG.DAOD_PHYS.e6337_s3681_r13145_p4926.TT_1L_March_3_v1_output_root/user.tdado.28320969._000008.output.root");
      }
      f->GetObject("nominal",tree);

   }
   Init(tree);
}

::~()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t ::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t ::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void ::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   mc_generator_weights = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_Continuous_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_60_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_60_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_60_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_60_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_70_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_70_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_77_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_77_eigenvars_Light_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_B_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_C_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_Light_up = 0;
   weight_bTagSF_DL1r_85_eigenvars_B_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_C_down = 0;
   weight_bTagSF_DL1r_85_eigenvars_Light_down = 0;
   el_pt = 0;
   el_eta = 0;
   el_cl_eta = 0;
   el_phi = 0;
   el_e = 0;
   el_charge = 0;
   el_CF = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_e = 0;
   mu_charge = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_e = 0;
   jet_jvt = 0;
   jet_truthPartonLabel = 0;
   jet_tagWeightBin_DL1r_Continuous = 0;
   jet_isbtagged_DL1r_60 = 0;
   jet_isbtagged_DL1r_70 = 0;
   jet_isbtagged_DL1r_77 = 0;
   jet_isbtagged_DL1r_85 = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mc_generator_weights", &mc_generator_weights, &b_mc_generator_weights);
   fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
   fChain->SetBranchAddress("weight_pileup", &weight_pileup, &b_weight_pileup);
   fChain->SetBranchAddress("weight_beamspot", &weight_beamspot, &b_weight_beamspot);
   fChain->SetBranchAddress("weight_leptonSF", &weight_leptonSF, &b_weight_leptonSF);
   fChain->SetBranchAddress("weight_trigger", &weight_trigger, &b_weight_trigger);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous", &weight_bTagSF_DL1r_Continuous, &b_weight_bTagSF_DL1r_Continuous);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60", &weight_bTagSF_DL1r_60, &b_weight_bTagSF_DL1r_60);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70", &weight_bTagSF_DL1r_70, &b_weight_bTagSF_DL1r_70);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77", &weight_bTagSF_DL1r_77, &b_weight_bTagSF_DL1r_77);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85", &weight_bTagSF_DL1r_85, &b_weight_bTagSF_DL1r_85);
   fChain->SetBranchAddress("weight_jvt", &weight_jvt, &b_weight_jvt);
   fChain->SetBranchAddress("weight_pileup_UP", &weight_pileup_UP, &b_weight_pileup_UP);
   fChain->SetBranchAddress("weight_pileup_DOWN", &weight_pileup_DOWN, &b_weight_pileup_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_UP", &weight_leptonSF_EL_SF_Reco_UP, &b_weight_leptonSF_EL_SF_Reco_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Reco_DOWN", &weight_leptonSF_EL_SF_Reco_DOWN, &b_weight_leptonSF_EL_SF_Reco_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_UP", &weight_leptonSF_EL_SF_ID_UP, &b_weight_leptonSF_EL_SF_ID_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_ID_DOWN", &weight_leptonSF_EL_SF_ID_DOWN, &b_weight_leptonSF_EL_SF_ID_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_UP", &weight_leptonSF_EL_SF_Isol_UP, &b_weight_leptonSF_EL_SF_Isol_UP);
   fChain->SetBranchAddress("weight_leptonSF_EL_SF_Isol_DOWN", &weight_leptonSF_EL_SF_Isol_DOWN, &b_weight_leptonSF_EL_SF_Isol_DOWN);
   fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_UP", &weight_trigger_EL_SF_Trigger_UP, &b_weight_trigger_EL_SF_Trigger_UP);
   fChain->SetBranchAddress("weight_trigger_EL_SF_Trigger_DOWN", &weight_trigger_EL_SF_Trigger_DOWN, &b_weight_trigger_EL_SF_Trigger_DOWN);
   fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_UP", &weight_trigger_MU_SF_STAT_UP, &b_weight_trigger_MU_SF_STAT_UP);
   fChain->SetBranchAddress("weight_trigger_MU_SF_STAT_DOWN", &weight_trigger_MU_SF_STAT_DOWN, &b_weight_trigger_MU_SF_STAT_DOWN);
   fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_UP", &weight_trigger_MU_SF_SYST_UP, &b_weight_trigger_MU_SF_SYST_UP);
   fChain->SetBranchAddress("weight_trigger_MU_SF_SYST_DOWN", &weight_trigger_MU_SF_SYST_DOWN, &b_weight_trigger_MU_SF_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_UP", &weight_leptonSF_MU_SF_ID_STAT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_UP", &weight_leptonSF_MU_SF_ID_SYST_UP, &b_weight_leptonSF_MU_SF_ID_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_DOWN", &weight_leptonSF_MU_SF_ID_SYST_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN", &weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN, &b_weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_UP", &weight_leptonSF_MU_SF_Isol_STAT_UP, &b_weight_leptonSF_MU_SF_Isol_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_STAT_DOWN", &weight_leptonSF_MU_SF_Isol_STAT_DOWN, &b_weight_leptonSF_MU_SF_Isol_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_UP", &weight_leptonSF_MU_SF_Isol_SYST_UP, &b_weight_leptonSF_MU_SF_Isol_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_Isol_SYST_DOWN", &weight_leptonSF_MU_SF_Isol_SYST_DOWN, &b_weight_leptonSF_MU_SF_Isol_SYST_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_UP", &weight_leptonSF_MU_SF_TTVA_STAT_UP, &b_weight_leptonSF_MU_SF_TTVA_STAT_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_STAT_DOWN", &weight_leptonSF_MU_SF_TTVA_STAT_DOWN, &b_weight_leptonSF_MU_SF_TTVA_STAT_DOWN);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_UP", &weight_leptonSF_MU_SF_TTVA_SYST_UP, &b_weight_leptonSF_MU_SF_TTVA_SYST_UP);
   fChain->SetBranchAddress("weight_leptonSF_MU_SF_TTVA_SYST_DOWN", &weight_leptonSF_MU_SF_TTVA_SYST_DOWN, &b_weight_leptonSF_MU_SF_TTVA_SYST_DOWN);
   fChain->SetBranchAddress("weight_jvt_UP", &weight_jvt_UP, &b_weight_jvt_UP);
   fChain->SetBranchAddress("weight_jvt_DOWN", &weight_jvt_DOWN, &b_weight_jvt_DOWN);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_up", &weight_bTagSF_DL1r_Continuous_eigenvars_B_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_up", &weight_bTagSF_DL1r_Continuous_eigenvars_C_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_up", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_up, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_B_down", &weight_bTagSF_DL1r_Continuous_eigenvars_B_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_C_down", &weight_bTagSF_DL1r_Continuous_eigenvars_C_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_Continuous_eigenvars_Light_down", &weight_bTagSF_DL1r_Continuous_eigenvars_Light_down, &b_weight_bTagSF_DL1r_Continuous_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_B_up", &weight_bTagSF_DL1r_60_eigenvars_B_up, &b_weight_bTagSF_DL1r_60_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_C_up", &weight_bTagSF_DL1r_60_eigenvars_C_up, &b_weight_bTagSF_DL1r_60_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_Light_up", &weight_bTagSF_DL1r_60_eigenvars_Light_up, &b_weight_bTagSF_DL1r_60_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_B_down", &weight_bTagSF_DL1r_60_eigenvars_B_down, &b_weight_bTagSF_DL1r_60_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_C_down", &weight_bTagSF_DL1r_60_eigenvars_C_down, &b_weight_bTagSF_DL1r_60_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_eigenvars_Light_down", &weight_bTagSF_DL1r_60_eigenvars_Light_down, &b_weight_bTagSF_DL1r_60_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_up", &weight_bTagSF_DL1r_60_extrapolation_up, &b_weight_bTagSF_DL1r_60_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_down", &weight_bTagSF_DL1r_60_extrapolation_down, &b_weight_bTagSF_DL1r_60_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_from_charm_up", &weight_bTagSF_DL1r_60_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_60_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_60_extrapolation_from_charm_down", &weight_bTagSF_DL1r_60_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_60_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_up", &weight_bTagSF_DL1r_70_eigenvars_B_up, &b_weight_bTagSF_DL1r_70_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_up", &weight_bTagSF_DL1r_70_eigenvars_C_up, &b_weight_bTagSF_DL1r_70_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_up", &weight_bTagSF_DL1r_70_eigenvars_Light_up, &b_weight_bTagSF_DL1r_70_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_B_down", &weight_bTagSF_DL1r_70_eigenvars_B_down, &b_weight_bTagSF_DL1r_70_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_C_down", &weight_bTagSF_DL1r_70_eigenvars_C_down, &b_weight_bTagSF_DL1r_70_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_eigenvars_Light_down", &weight_bTagSF_DL1r_70_eigenvars_Light_down, &b_weight_bTagSF_DL1r_70_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_up", &weight_bTagSF_DL1r_70_extrapolation_up, &b_weight_bTagSF_DL1r_70_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_down", &weight_bTagSF_DL1r_70_extrapolation_down, &b_weight_bTagSF_DL1r_70_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_up", &weight_bTagSF_DL1r_70_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_70_extrapolation_from_charm_down", &weight_bTagSF_DL1r_70_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_70_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_up", &weight_bTagSF_DL1r_77_eigenvars_B_up, &b_weight_bTagSF_DL1r_77_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_up", &weight_bTagSF_DL1r_77_eigenvars_C_up, &b_weight_bTagSF_DL1r_77_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_up", &weight_bTagSF_DL1r_77_eigenvars_Light_up, &b_weight_bTagSF_DL1r_77_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_B_down", &weight_bTagSF_DL1r_77_eigenvars_B_down, &b_weight_bTagSF_DL1r_77_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_C_down", &weight_bTagSF_DL1r_77_eigenvars_C_down, &b_weight_bTagSF_DL1r_77_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_eigenvars_Light_down", &weight_bTagSF_DL1r_77_eigenvars_Light_down, &b_weight_bTagSF_DL1r_77_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_up", &weight_bTagSF_DL1r_77_extrapolation_up, &b_weight_bTagSF_DL1r_77_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_down", &weight_bTagSF_DL1r_77_extrapolation_down, &b_weight_bTagSF_DL1r_77_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_up", &weight_bTagSF_DL1r_77_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_77_extrapolation_from_charm_down", &weight_bTagSF_DL1r_77_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_77_extrapolation_from_charm_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_B_up", &weight_bTagSF_DL1r_85_eigenvars_B_up, &b_weight_bTagSF_DL1r_85_eigenvars_B_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_C_up", &weight_bTagSF_DL1r_85_eigenvars_C_up, &b_weight_bTagSF_DL1r_85_eigenvars_C_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_Light_up", &weight_bTagSF_DL1r_85_eigenvars_Light_up, &b_weight_bTagSF_DL1r_85_eigenvars_Light_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_B_down", &weight_bTagSF_DL1r_85_eigenvars_B_down, &b_weight_bTagSF_DL1r_85_eigenvars_B_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_C_down", &weight_bTagSF_DL1r_85_eigenvars_C_down, &b_weight_bTagSF_DL1r_85_eigenvars_C_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_eigenvars_Light_down", &weight_bTagSF_DL1r_85_eigenvars_Light_down, &b_weight_bTagSF_DL1r_85_eigenvars_Light_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_up", &weight_bTagSF_DL1r_85_extrapolation_up, &b_weight_bTagSF_DL1r_85_extrapolation_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_down", &weight_bTagSF_DL1r_85_extrapolation_down, &b_weight_bTagSF_DL1r_85_extrapolation_down);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_from_charm_up", &weight_bTagSF_DL1r_85_extrapolation_from_charm_up, &b_weight_bTagSF_DL1r_85_extrapolation_from_charm_up);
   fChain->SetBranchAddress("weight_bTagSF_DL1r_85_extrapolation_from_charm_down", &weight_bTagSF_DL1r_85_extrapolation_from_charm_down, &b_weight_bTagSF_DL1r_85_extrapolation_from_charm_down);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("randomRunNumber", &randomRunNumber, &b_randomRunNumber);
   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mu", &mu, &b_mu);
   fChain->SetBranchAddress("mu_actual", &mu_actual, &b_mu_actual);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_cl_eta", &el_cl_eta, &b_el_cl_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_e", &el_e, &b_el_e);
   fChain->SetBranchAddress("el_charge", &el_charge, &b_el_charge);
   fChain->SetBranchAddress("el_CF", &el_CF, &b_el_CF);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_e", &mu_e, &b_mu_e);
   fChain->SetBranchAddress("mu_charge", &mu_charge, &b_mu_charge);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_e", &jet_e, &b_jet_e);
   fChain->SetBranchAddress("jet_jvt", &jet_jvt, &b_jet_jvt);
   fChain->SetBranchAddress("jet_truthPartonLabel", &jet_truthPartonLabel, &b_jet_truthPartonLabel);
   fChain->SetBranchAddress("jet_tagWeightBin_DL1r_Continuous", &jet_tagWeightBin_DL1r_Continuous, &b_jet_tagWeightBin_DL1r_Continuous);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_60", &jet_isbtagged_DL1r_60, &b_jet_isbtagged_DL1r_60);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_70", &jet_isbtagged_DL1r_70, &b_jet_isbtagged_DL1r_70);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_77", &jet_isbtagged_DL1r_77, &b_jet_isbtagged_DL1r_77);
   fChain->SetBranchAddress("jet_isbtagged_DL1r_85", &jet_isbtagged_DL1r_85, &b_jet_isbtagged_DL1r_85);
   fChain->SetBranchAddress("met_met", &met_met, &b_met_met);
   fChain->SetBranchAddress("met_sumet", &met_sumet, &b_met_sumet);
   fChain->SetBranchAddress("met_phi", &met_phi, &b_met_phi);
   fChain->SetBranchAddress("ejets_2015", &ejets_2015, &b_ejets_2015);
   fChain->SetBranchAddress("ejets_2016", &ejets_2016, &b_ejets_2016);
   fChain->SetBranchAddress("ejets_2017", &ejets_2017, &b_ejets_2017);
   fChain->SetBranchAddress("ejets_2018", &ejets_2018, &b_ejets_2018);
   fChain->SetBranchAddress("mujets_2015", &mujets_2015, &b_mujets_2015);
   fChain->SetBranchAddress("mujets_2016", &mujets_2016, &b_mujets_2016);
   fChain->SetBranchAddress("mujets_2017", &mujets_2017, &b_mujets_2017);
   fChain->SetBranchAddress("mujets_2018", &mujets_2018, &b_mujets_2018);
   fChain->SetBranchAddress("hadBjetIndex", &hadBjetIndex, &b_hadBjetIndex);
   fChain->SetBranchAddress("lightJetIndex1", &lightJetIndex1, &b_lightJetIndex1);
   fChain->SetBranchAddress("lightJetIndex2", &lightJetIndex2, &b_lightJetIndex2);
   fChain->SetBranchAddress("chi2", &chi2, &b_chi2);
   Notify();
}

Bool_t ::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void ::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t ::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef _cxx
