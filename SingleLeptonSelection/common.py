from ROOT import *
from array import array

# From Tomas lumi
# lumi_18 = 58450.1

# hence my luminosity used is
#iLumi   = 225.904 # Period D-E
iLumi   = 792.315 # Period F1
#iLumi   = 727.059 #temporary lumi for 1L/2L partial data

nb = 1e3
pb = 1.
fb = 1e-3

###### Cross-section values, need to cross-check each entry from XSection-MC16-13TeV.data
## only Top 410470 was modified

xs = {
   # SingleTop
   # s-channel
   '601348': 1.3525 * 1.0872*pb, #Powheg v2 + Pythia8
   '601349': 2.1455 * 1.0963*pb, #Powheg v2 + Pythia8
   # t-channel
   '601350': 24.203 * 1.0858*pb, #Powheg v2 + Pythia8
   '601351': 39.936 * 1.1066*pb, #Powheg v2 + Pythia8
   # tW inclusive
   '601352': 39.839 * 0.9924*pb, #Powheg v2 + Pythia8
   '601355': 39.876 * 0.9924*pb, #Powheg v2 + Pythia8
   # tW dilepton
   '601353': 4.1974 * 0.9924*pb, #Powheg v2 + Pythia8
   '601354': 4.2013 * 0.9924*pb, #Powheg v2 + Pythia8
   
   
   # Sherpa 2.2.10-12 diboson
   # "Semileptonic" VV sherpa 2.2.12 - not really semileptonic...
   '700566': 51.226 * 0.91 * pb, #sherpa2210
   '700567': 2.5961 * 0.91 * pb, #sherpa2210
   '700568': 9.2309 * 0.91 * pb, #sherpa2210
   '700569': 3.5549 * 0.91 * pb, #sherpa2210
   '700570': 7.0361 * 0.91 * pb, #sherpa2210
   '700571': 0.4986 * 0.91 * pb, #sherpa2210
   '700572': 1.7741 * 0.91 * pb, #sherpa2210
   '700573': 0.9876 * 0.91 * pb, #sherpa2210
   '700574': 3.5125 * 0.91 * pb, #sherpa2210
   # Multilepton+neutrino VV sherpa 2.2.12
   '700600': 1.3351 * 0.91 * pb, #sherpa2210
   '700601': 4.8181 * 0.91 * pb, #sherpa2210
   '700602': 12.618 * 0.91 * pb, #sherpa2210
   '700603': 0.0241 * 0.91 * pb, #sherpa2210
   '700604': 3.289 * 0.91 * pb, #sherpa2210
   '700605': 0.61379 * 0.91 * pb, #sherpa2210


   #TTBAR
   '601229': 355.63972 * 1.1381 * pb,
   '601230': 85.482 * 1.1381 * pb, # ttbar dilep
   '601237': 369.95387 * 1.1381 * pb, # Pythia8


   #ttbar modelling
   # '411233': 320.1853 * 1.1392*pb, #herwigpp713
   # '411234': 77.01622 * 1.1391*pb, #herwigpp713
   # '412116': 313.2281 * 1.1694*pb, #herwigpp713
   # '412117': 76.2908 * 1.1681*pb, #herwigpp713

   
   #Zjets

   # Z(ee)+jets sherpa 2.2.12
   '700615': 59.841 * 0.9383 * pb, #sherpa2210
   '700616': 303.25 * 0.9383 * pb, #sherpa2210
   '700617': 1974.0 * 0.9383 * pb, #sherpa2210
   # Z(mumu)+jets sherpa 2.2.12
   '700618': 58.688 * 0.9383 * pb, #sherpa2210
   '700619': 304.48 * 0.9383 * pb, #sherpa2210
   '700620': 1974.2 * 0.9383 * pb, #sherpa2210
   # Z(tautau)+jets sherpa 2.2.12
   '700621': 59.655 * 0.9383 * pb, #sherpa2210
   '700622': 302.53 * 0.9383 * pb, #sherpa2210
   '700623': 1971.0 * 0.9383 * pb, #sherpa2210
   # Z(nunu)+jets sherpa 2.2.12
   '700624': 41.121 * 1.0 * pb, #sherpa2210
   '700625': 97.944 * 1.0 * pb, #sherpa2210
   '700626': 342.52 * 1.0 * pb, #sherpa2210
   # Z+jets 10 < m_ll < 40 sherpa 2.2.12
   '700575': 4266.4 * 1.0 * pb, #sherpa2210
   '700576': 4266.3 * 1.0 * pb, #sherpa2210
   '700577': 4265.6 * 1.0 * pb, #sherpa2210

   
   
   #Wjets
   # W(->e nu)+jets sherpa 2.2.12 
   '700606': 220.132 * 0.9059 * pb, #sherpa2210
   '700607': 3402.25 * 0.9059 * pb, #sherpa2210
   '700608': 19290.2 * 0.9059 * pb, #sherpa2210
   # W(->mu nu)+jets sherpa 2.2.12
   '700609': 218.007 * 0.9059 * pb, #sherpa2210
   '700610': 3411.47 * 0.9059 * pb, #sherpa2210
   '700611': 19326.7 * 0.9059 * pb, #sherpa2210
   # W(->tau nu)+jets sherpa 2.2.12
   '700612': 220.519 * 0.9059 * pb, #sherpa2210
   '700613': 3411.31 * 0.9059 * pb, #sherpa2210
   '700614': 19337.6 * 0.9059 * pb, #sherpa2210
  
  # Top Rel 22.
  # https://gitlab.cern.ch/tdado/ttbarxsecrun3/-/blob/master/data/XSection-MC16-13TeV.data says 410470 396.87 1.1398 pythia8
  #, so we multiply 396.87 x 1.1398 = 452.35243 - this is still 13 TeV
  # in principle it should be equal to the THEO> cross-section times BR
  # Branching ratio is: 1-(1-3*0.1082)^2=0.5438, 
  # THEO crosssection is (https://indico.cern.ch/event/1144393/contributions/4804148/attachments/2418904/4140014/Talk.pdf) 
  # at 13.6 TeV is 923 pb
  # at 13 TeV is 832 pb
  # multiplying those guys we have for 13 TeV we have 452.44160 which is almost 452.35243 (can ask Tomas why they are not exactly the same) 
}

#####################

GeV = 1e3
TeV = 1e6
m_W_PDG   = 80.4*GeV
m_top_PDG = 172.5*GeV

#####################

systematics_tree = [
   "nominal",
   "CategoryReduction_JET_BJES_Response__1down",
   "CategoryReduction_JET_BJES_Response__1up",
   "CategoryReduction_JET_EffectiveNP_Detector1__1down",
   "CategoryReduction_JET_EffectiveNP_Detector1__1up",
   "CategoryReduction_JET_EffectiveNP_Detector2__1down",
   "CategoryReduction_JET_EffectiveNP_Detector2__1up",
   "CategoryReduction_JET_EffectiveNP_Mixed1__1down",
   "CategoryReduction_JET_EffectiveNP_Mixed1__1up",
   "CategoryReduction_JET_EffectiveNP_Mixed2__1down",
   "CategoryReduction_JET_EffectiveNP_Mixed2__1up",
   "CategoryReduction_JET_EffectiveNP_Mixed3__1down",
   "CategoryReduction_JET_EffectiveNP_Mixed3__1up",
   "CategoryReduction_JET_EffectiveNP_Modelling1__1down",
   "CategoryReduction_JET_EffectiveNP_Modelling1__1up",
   "CategoryReduction_JET_EffectiveNP_Modelling2__1down",
   "CategoryReduction_JET_EffectiveNP_Modelling2__1up",
   "CategoryReduction_JET_EffectiveNP_Modelling3__1down",
   "CategoryReduction_JET_EffectiveNP_Modelling3__1up",
   "CategoryReduction_JET_EffectiveNP_Modelling4__1down",
   "CategoryReduction_JET_EffectiveNP_Modelling4__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical1__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical1__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical2__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical2__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical3__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical3__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical4__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical4__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical5__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical5__1up",
   "CategoryReduction_JET_EffectiveNP_Statistical6__1down",
   "CategoryReduction_JET_EffectiveNP_Statistical6__1up",
   "CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
   "CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down",
   "CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up",
   "CategoryReduction_JET_EtaIntercalibration_TotalStat__1down",
   "CategoryReduction_JET_EtaIntercalibration_TotalStat__1up",
   "CategoryReduction_JET_Flavor_Composition__1down",
   "CategoryReduction_JET_Flavor_Composition__1up",
   "CategoryReduction_JET_Flavor_Response__1down",
   "CategoryReduction_JET_Flavor_Response__1up",
   "CategoryReduction_JET_Pileup_OffsetMu__1down",
   "CategoryReduction_JET_Pileup_OffsetMu__1up",
   "CategoryReduction_JET_Pileup_OffsetNPV__1down",
   "CategoryReduction_JET_Pileup_OffsetNPV__1up",
   "CategoryReduction_JET_Pileup_PtTerm__1down",
   "CategoryReduction_JET_Pileup_PtTerm__1up",
   "CategoryReduction_JET_Pileup_RhoTopology__1down",
   "CategoryReduction_JET_Pileup_RhoTopology__1up",
   "CategoryReduction_JET_PunchThrough_MC16__1down",
   "CategoryReduction_JET_PunchThrough_MC16__1up",
   "CategoryReduction_JET_SingleParticle_HighPt__1down",
   "CategoryReduction_JET_SingleParticle_HighPt__1up",
   "CategoryReduction_JET_JER_DataVsMC_MC16__1down",
   "CategoryReduction_JET_JER_DataVsMC_MC16__1up",
   "CategoryReduction_JET_JER_EffectiveNP_1__1down",
   "CategoryReduction_JET_JER_EffectiveNP_1__1up",
   "CategoryReduction_JET_JER_EffectiveNP_2__1down",
   "CategoryReduction_JET_JER_EffectiveNP_2__1up",
   "CategoryReduction_JET_JER_EffectiveNP_3__1down",
   "CategoryReduction_JET_JER_EffectiveNP_3__1up",
   "CategoryReduction_JET_JER_EffectiveNP_4__1down",
   "CategoryReduction_JET_JER_EffectiveNP_4__1up",
   "CategoryReduction_JET_JER_EffectiveNP_5__1down",
   "CategoryReduction_JET_JER_EffectiveNP_5__1up",
   "CategoryReduction_JET_JER_EffectiveNP_6__1down",
   "CategoryReduction_JET_JER_EffectiveNP_6__1up",
   "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down",
   "CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up",
   "EG_RESOLUTION_ALL__1down",
   "EG_RESOLUTION_ALL__1up",
   "EG_SCALE_AF2__1down",
   "EG_SCALE_AF2__1up",
   "EG_SCALE_ALL__1down",
   "EG_SCALE_ALL__1up",
   "MET_SoftTrk_ResoPara",
   "MET_SoftTrk_ResoPerp",
   "MET_SoftTrk_ScaleDown",
   "MET_SoftTrk_ScaleUp",
   "MUON_ID__1down",
   "MUON_ID__1up",
   "MUON_MS__1down",
   "MUON_MS__1up",
   "MUON_SAGITTA_RESBIAS__1down",
   "MUON_SAGITTA_RESBIAS__1up",
   "MUON_SAGITTA_RHO__1down",
   "MUON_SAGITTA_RHO__1up",
   "MUON_SCALE__1down",
   "MUON_SCALE__1up",
]

systematics_btagging_DL1_77 = [
   "bTagSF_DL1_77_eigenvars_B_down",
   "bTagSF_DL1_77_eigenvars_B_up",
   "bTagSF_DL1_77_eigenvars_C_down",
   "bTagSF_DL1_77_eigenvars_C_up",
   "bTagSF_DL1_77_eigenvars_Light_down",
   "bTagSF_DL1_77_eigenvars_Light_up",
   "bTagSF_DL1_77_extrapolation_down",
   "bTagSF_DL1_77_extrapolation_up",
   "bTagSF_DL1_77_extrapolation_from_charm_down",
   "bTagSF_DL1_77_extrapolation_from_charm_up",
]


systematics_weight = [
   "jvt_DOWN",
   "jvt_UP",
   "pileup_DOWN",
   "pileup_UP",
   "indiv_SF_EL_ChargeID",
   "indiv_SF_EL_ChargeID_DOWN",
   "indiv_SF_EL_ChargeID_UP",
   "indiv_SF_EL_ChargeMisID",
   "indiv_SF_EL_ChargeMisID_STAT_DOWN",
   "indiv_SF_EL_ChargeMisID_STAT_UP",
   "indiv_SF_EL_ChargeMisID_SYST_DOWN",
   "indiv_SF_EL_ChargeMisID_SYST_UP",
   "indiv_SF_EL_ID_DOWN",
   "indiv_SF_EL_ID_UP",
   "indiv_SF_EL_Isol_DOWN",
   "indiv_SF_EL_Isol_UP",
   "indiv_SF_EL_Reco_DOWN",
   "indiv_SF_EL_Reco_UP",
   "indiv_SF_EL_Trigger_DOWN",
   "indiv_SF_EL_Trigger_UP",
   "indiv_SF_MU_ID_STAT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_DOWN",
   "indiv_SF_MU_ID_STAT_LOWPT_UP",
   "indiv_SF_MU_ID_STAT_UP",
   "indiv_SF_MU_ID_SYST_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_DOWN",
   "indiv_SF_MU_ID_SYST_LOWPT_UP",
   "indiv_SF_MU_ID_SYST_UP",
   "indiv_SF_MU_Isol_STAT_DOWN",
   "indiv_SF_MU_Isol_STAT_UP",
   "indiv_SF_MU_Isol_SYST_DOWN",
   "indiv_SF_MU_Isol_SYST_UP",
   "indiv_SF_MU_TTVA_STAT_DOWN",
   "indiv_SF_MU_TTVA_STAT_UP",
   "indiv_SF_MU_TTVA_SYST_DOWN",
   "indiv_SF_MU_TTVA_SYST_UP",
   "indiv_SF_MU_Trigger_STAT_DOWN",
   "indiv_SF_MU_Trigger_STAT_UP",
   "indiv_SF_MU_Trigger_SYST_DOWN",
   "indiv_SF_MU_Trigger_SYST_UP",
   "leptonSF_EL_SF_ID_DOWN",
   "leptonSF_EL_SF_ID_UP",
   "leptonSF_EL_SF_Isol_DOWN",
   "leptonSF_EL_SF_Isol_UP",
   "leptonSF_EL_SF_Reco_DOWN",
   "leptonSF_EL_SF_Reco_UP",
   "trigger_EL_SF_Trigger_DOWN",
   "trigger_EL_SF_Trigger_UP",
   "leptonSF_MU_SF_ID_STAT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_STAT_LOWPT_UP",
   "leptonSF_MU_SF_ID_STAT_UP",
   "leptonSF_MU_SF_ID_SYST_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_DOWN",
   "leptonSF_MU_SF_ID_SYST_LOWPT_UP",
   "leptonSF_MU_SF_ID_SYST_UP",
   "leptonSF_MU_SF_Isol_STAT_DOWN",
   "leptonSF_MU_SF_Isol_STAT_UP",
   "leptonSF_MU_SF_Isol_SYST_DOWN",
   "leptonSF_MU_SF_Isol_SYST_UP",
   "leptonSF_MU_SF_TTVA_STAT_DOWN",
   "leptonSF_MU_SF_TTVA_STAT_UP",
   "leptonSF_MU_SF_TTVA_SYST_DOWN",
   "leptonSF_MU_SF_TTVA_SYST_UP",
   "trigger_MU_SF_Trigger_STAT_DOWN",
   "trigger_MU_SF_Trigger_STAT_UP",
   "trigger_MU_SF_Trigger_SYST_DOWN",
   "trigger_MU_SF_Trigger_SYST_UP",
   ]


#####################

known_channels = [ "ljets",  "ejets", "mujets" ]

known_regions = ["4jetin1bin",
 "4jetin0bex", "4jetin1bex", "4jetin2bex", "4jetin3bin",
]
 
#####################
histograms = {}
for channel in known_channels:
   histograms[channel] = {}
   for region in known_regions:
      histograms[channel][region] = {}

      ### main fit 
      histograms[channel][region]['HT500']= TH1F( "%s_%s_HT500"%(channel,region),   ";HT_{had} [GeV]" , 100, 0., 1500.  )
      histograms[channel][region]['HT750']= TH1F( "%s_%s_HT750"%(channel,region),   ";HT_{had} [GeV]" , 150, 0., 1750.  )

      histograms[channel][region]['FW2']   = TH1F( "%s_%s_FW2"%(channel,region),   ";FW2"  , 100, 0., 1. )

      histograms[channel][region]['median_dR_bl_bin4']  = TH1F( "%s_%s_median_dR_bl_bin4"%(channel,region),  ";#Delta R_{bl} (med.)" , 100, 0.4, 4.0 )
      histograms[channel][region]['median_dR_bl_bin5']  = TH1F( "%s_%s_median_dR_bl_bin5"%(channel,region),  ";#Delta R_{bl} (med.)" , 100, 0.4, 5.0 )

      histograms[channel][region]['dR_jj_med'] = TH1F( "%s_%s_dR_jj_med"%(channel,region),  ";#Delta R_{jj} (med.)}"   , 100, 0.4, 5.0 )
      histograms[channel][region]['dR_uu_med'] = TH1F( "%s_%s_dR_uu_med"%(channel,region),  ";#Delta R_{uu} (med.)}"   , 100, 0.4, 4.0 )

      histograms[channel][region]['muu_mindR'] = TH1F( "%s_%s_muu_mindR"%(channel,region), ";m(uu)^{min dR} [GeV]" , 100, 0., 200. )
      histograms[channel][region]['mjj_mindR'] = TH1F( "%s_%s_mjj_mindR"%(channel,region), ";m(jj)^{min dR} [GeV]" , 100, 0., 200. )



      # BDT output
      histograms[channel][region]['BDTG_opt3_trained_in_CR']  = TH1F( "%s_%s_BDTG_opt3_CR"%(channel,region), ";BDTG opt3 trained in CR", 100, -1., 1. )
      histograms[channel][region]['BDTG_opt3_trained_in_SR']  = TH1F( "%s_%s_BDTG_opt3_SR"%(channel,region), ";BDTG opt3 trained in SR", 100, -1., 1. )

      


      ### kinematics
      histograms[channel][region]['lep_eta']  = TH1F( "%s_%s_lep_eta"%(channel,region),  ";Lepton #eta"   , 50, -2.5, 2.5 )
      histograms[channel][region]['met']  = TH1F( "%s_%s_met"%(channel,region),  ";E_{T}^{miss} [GeV]", 50, 0., 300. )
      histograms[channel][region]['mtw_lep']  = TH1F( "%s_%s_mtw_lep"%(channel,region),  ";m_{T}^{l,v} [GeV]" , 50, 0., 300. )
      histograms[channel][region]['lep_pt']   = TH1F( "%s_%s_lep_pt"%(channel,region),   ";Lepton p_{T} [GeV]", 50, 0., 300. )
      histograms[channel][region]['lep_q']= TH1F( "%s_%s_lep_q"%(channel,region),";Lepton charge" , 3, -1.5, 1.5 )
      histograms[channel][region]['lep_phi']  = TH1F( "%s_%s_lep_phi"%(channel,region),  ";Lepton #phi"   , 50, -TMath.Pi(), TMath.Pi() )
      histograms[channel][region]['dPhi_j1_lep'] = TH1F( "%s_%s_dPhi_j1_lep"%(channel,region), ";#Delta#phi(lep,j1)", 16, -TMath.Pi(), TMath.Pi() )


      histograms[channel][region]['j1_pt']= TH1F( "%s_%s_j1_pt"%(channel,region), ";1st jet p_{T} [GeV]", 50, 0., 500. )
      histograms[channel][region]['j2_pt']= TH1F( "%s_%s_j2_pt"%(channel,region), ";2nd jet p_{T} [GeV]", 50, 0., 400. )
      histograms[channel][region]['j3_pt']= TH1F( "%s_%s_j3_pt"%(channel,region), ";3rd jet p_{T} [GeV]", 50, 0., 300. )
      histograms[channel][region]['j4_pt']= TH1F( "%s_%s_j4_pt"%(channel,region), ";4th jet p_{T} [GeV]", 50, 0., 200. )
      histograms[channel][region]['j1_eta']   = TH1F( "%s_%s_j1_eta"%(channel,region),";1st jet #eta", 50, -2.5, 2.5 )
      histograms[channel][region]['j2_eta']   = TH1F( "%s_%s_j2_eta"%(channel,region),";2nd jet #eta", 50, -2.5, 2.5 )
      histograms[channel][region]['j3_eta']   = TH1F( "%s_%s_j3_eta"%(channel,region),";3rd jet #eta", 50, -2.5, 2.5 )
      histograms[channel][region]['j4_eta']   = TH1F( "%s_%s_j4_eta"%(channel,region),";4th jet #eta", 50, -2.5, 2.5 )
      histograms[channel][region]['j1_phi']   = TH1F( "%s_%s_j1_phi"%(channel,region),";1st jet #phi", 50, -TMath.Pi(), TMath.Pi() )
      histograms[channel][region]['j2_phi']   = TH1F( "%s_%s_j2_phi"%(channel,region),";2nd jet #phi", 50, -TMath.Pi(), TMath.Pi() )
      histograms[channel][region]['j3_phi']   = TH1F( "%s_%s_j3_phi"%(channel,region),";3rd jet #phi", 50, -TMath.Pi(), TMath.Pi() )
      histograms[channel][region]['j4_phi']   = TH1F( "%s_%s_j4_phi"%(channel,region),";4th jet #phi", 50, -TMath.Pi(), TMath.Pi() )

      ### plus 
      histograms[channel][region]['HT500_plus']  = TH1F( "%s_%s_HT500_plus"%(channel,region),   ";HT_{had} [GeV]" , 100, 0., 500.  )
      histograms[channel][region]['HT750_plus']  = TH1F( "%s_%s_HT750_plus"%(channel,region),   ";HT_{had} [GeV]" , 150, 0., 750.  )
      histograms[channel][region]['BDTG_opt3_trained_in_CR_plus']  = TH1F( "%s_%s_BDTG_opt3_CR_plus"%(channel,region), ";BDTG opt3 trained in CR", 100, -1., 1. )
      histograms[channel][region]['BDTG_opt3_trained_in_SR_plus']  = TH1F( "%s_%s_BDTG_opt3_SR_plus"%(channel,region), ";BDTG opt3 trained in SR", 100, -1., 1. )

      ### minus
      histograms[channel][region]['HT500_minus']= TH1F( "%s_%s_HT500_minus"%(channel,region),";HT_{had} [GeV]" , 100, 0., 500.  )
      histograms[channel][region]['HT750_minus']= TH1F( "%s_%s_HT750_minus"%(channel,region),";HT_{had} [GeV]" , 150, 0., 750.  )
      histograms[channel][region]['BDTG_opt3_trained_in_CR_minus']  = TH1F( "%s_%s_BDTG_opt3_CR_minus"%(channel,region), ";BDTG opt3 trained in CR", 100, -1., 1. )
      histograms[channel][region]['BDTG_opt3_trained_in_SR_minus']  = TH1F( "%s_%s_BDTG_opt3_SR_minus"%(channel,region), ";BDTG opt3 trained in SR", 100, -1., 1. )

      ## njets
      histograms[channel][region]['jets_N']   = TH1F( "%s_%s_jets_n"%(channel,region),  ";Jets multiplicity", 8, 0.5, 8.5 )
      histograms[channel][region]['bjets_N']  = TH1F( "%s_%s_bjets_n"%(channel,region), ";b-jets multiplicity"  , 4, -0.5, 3.5 )






      for h in histograms[channel][region].values(): h.Sumw2()

  
  
#####################
