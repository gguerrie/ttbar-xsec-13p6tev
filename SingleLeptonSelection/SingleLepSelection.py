#!/usr/bin/env python
import os, sys, time
import argparse

timer_start = time.time()

from array import array
from numpy import median

import ROOT as rt

from common import *
import helper_functions

########################################################
parser = argparse.ArgumentParser(description='ttbar 13.6 TeV: Single Lepton selection maker')
parser.add_argument( '-i', '--filelistname', default="filelists/check.txt" )
parser.add_argument( '-o', '--outfilename',  default="" )
parser.add_argument( '-s', '--systematic',   default="nominal" )
parser.add_argument( '-p', '--preselection', default="check" )
parser.add_argument( '-j', '--jetcut',       default="25" )    # default is 20 GeV
parser.add_argument( '-HF', '--Wjets_HF' ,   default="all" )   # l, c, b, 1bTruth, 2bTruth

#### we are using 77% wp now
parser.add_argument( '-b', '--btagcut',      default="77" )    # we are cutting on DL1dv01, see selection file JET_N_BTAG DL1dv01:FixedCutBEff_77 >= 1
 
args         = parser.parse_args()
filelistname = args.filelistname
outfilename  = args.outfilename
syst         = args.systematic
preselection = args.preselection
jetcut       = args.jetcut
btagcut      = args.btagcut
Wjets_HF     = args.Wjets_HF

# check what is data and what is not
isData = False
if filelistname.find('data') > -1: isData = True

isQCD = False
if filelistname.find('qcd') > -1:
    isData = True
    isQCD = True

isWjets = False
if filelistname.find('Wjets') > -1: isWjets = True



# check I am reading a filelist
if filelistname == "":
    print "ERROR: please specify a file list"
    exit(1)

# default name of the tree
treename = "nominal"

## here begins the systs change, in case it is not nominal
if not isData:
    # search in common.py for all the systs defined in systematics_tree
    if syst in systematics_tree:
        treename = syst # change the name of the tree to be read
    else:
        treename = "nominal"

if isQCD:
    treename = "nominal_Loose"

print "INFO: running filelistname: ", filelistname
print "INFO: running preselection: ", preselection
print "INFO: running systematic  : ", syst
print "INFO: reading TTree                               : ", treename
print "INFO: using jet pT cut                            : ", float(jetcut)
print "INFO: using jet WP cut                            : ", float(btagcut)
if isWjets:
    print "INFO: using W+jets HF (l=light, c or b-jets, or truth jets) cut    : ", Wjets_HF



################################################################################

# Get ROOT trees
tree = rt.TChain( treename, treename )
for fname in open( filelistname, 'r' ).readlines():
    fname = fname.strip()
    tree.Add( fname )


# output name
ext = filelistname.split('/')[-1].split('.')[-1]

# need to check what happens for systs in Wjets splitting
if outfilename == "":
    outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.root"%syst)
    if isWjets:
        outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.%s.root"%(Wjets_HF,syst))

    if syst == "nominal":
        outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "root")
        if isWjets:
            outfilename = "output/" + preselection + "/" + filelistname.split('/')[-1].replace( ext, "%s.root"%Wjets_HF)


############ 
print "INFO: output saved  in                            : ", outfilename
outfile = rt.TFile.Open( outfilename, "RECREATE" )


############# 


# Event loop
nentries = tree.GetEntries()


# lower number of events
#nentries=100000

print "INFO: looping over %i entries" % nentries

#sys.exit()

sum_weights=0

for ientry in range(nentries):
    tree.GetEntry( ientry )

    # print INFO
    eventNumber     = tree.eventNumber
    runNumber       = tree.runNumber
    mcChannelNumber = tree.mcChannelNumber

    if ( nentries < 10 ) or ( (ientry+1) % int(float(nentries)/10.)                == 0 ):
        perc = 100. * ientry / float(nentries)
        print "INFO: Event %-9i (event number = %-10i run number = %-10i )   (%3.0f %%)" % ( ientry, eventNumber, runNumber, perc )

    #print "INFO: Event %-9i (event number = %-10i run number = %-10i )   " % ( ientry, eventNumber, runNumber )

    jet_pt_cut     = float(jetcut)
    b_tag_cut      = float(btagcut)


    # check pre-selection, watch out as I only ask 2022 here
    passed_ejets_2022  = tree.ejets_2022       #
    passed_mujets_2022 = tree.mujets_2022      #

    channel = 0
    if passed_ejets_2022 == 1: channel = 11
    # No e and mu
    if passed_mujets_2022 == 1:
        if channel == 11: continue
        else: channel = 13

    accepted = ( channel > 0 )
    if not accepted: continue


    # Identify lepton
    ch_label = ""

    if channel == 11:
        #print "INFO: Event %-9i marked as electron  " % ( ientry )
        lep = helper_functions.MakeEventElectron( tree )
        ch_label += "ejets"

    elif channel == 13:
        lep = helper_functions.MakeEventMuon( tree )
        ch_label += "mujets"

    #print "INFO: lepton flavour is: ", ch_label

    # if ientry == 3734 or ientry == 3735 or ientry == 12488 or ientry == 15087 or ientry == 17846 or ientry == 19357:
    #     print("Electron event number: "+ str(ientry) + "-----")
    #     print(lep.N)
    #     print(lep.Pt()/GeV)
    #     print(lep.Crack)
    #     print(len(tree.jet_pt))


    # Default cut on the number of lepton == 1, this way we remove dilepton events
    # if lep.N != 1:
    #     continue

    # cut on lepton pT is back
    #if lep.Pt()/GeV < 25: continue

    # Crack electrons are NOT accepted 
    #if lep.Crack == True:
    #   continue 


    ###################################################################################################
    # syst and nominal have different weights stored in tree!
    # if syst is not specified, syst=nominal
    w = 1.
    if not mcChannelNumber == 0:
        dsid = str(tree.mcChannelNumber) # read the DSID
        w *= tree.weight_mc  # multiply by MC weight by default
        w_total, w_extra = helper_functions.GetEventWeight( lep, tree, syst, b_tag_cut, dsid ) # here comes the magic of the event weight, look in helper_functionsSS
        #print "weight_mc: ", tree.weight_mc
        w *= w_total
        #print "Common product 1234567: ", w
        # print "iLumi: ", iLumi
        # print "xs: ", xs[dsid]
        # print "Nominal norm: ", iLumi * xs[dsid]
        #print "--- new event --- "
        #print "CHECK: by default multiply by weight_mc: ", tree.weight_mc

        

        #print "Nominal norm: ", iLumi * xs[dsid]
        w *= iLumi * xs[dsid] # multiply by luminosity and by the cross-section of the sample

        #print "Nominal weight for this event: ", w

        sum_weights +=w

    #w = 1.

    ###################################################################################################
    # QCD magic (in principle whould work in Rel.22 when fake esitmation is available)
    # if isQCD:
    #    w *= tree.ASM_weight[0]


    ###################################################################################################
    # regions
    jets_n  = len(tree.jet_pt)
    n_bjets = len(tree.jet_pt)

    ############################################################
    jets, bjets = helper_functions.MakeEventJets( tree, jet_pt_cut,  b_tag_cut)

    jets_n  = len( jets )
    n_bjets = len( bjets )

    #print "INFO: selection on jets: Njets = %i and Nbjets = %i"  % ( jets_n, n_bjets )


    ###################################################################################################
    # cut on nJets 
    #if jets_n < 4: continue


    ###################################################################################################
    # split W+jets into W+light, W+ at least one c and W+ at least 1b (W+HF together)
    # amazing but Tomas did not store this info!!!
    if isWjets:

        jets_flavour = []

    ###     for i in range(jets_n):
    ###         #print "INFO: jet truth flavour of jet is: ", tree.jet_truthflav[i]
    ###        jets_flavour += [ tree.jet_truthflav[i] ]

        WjetsFlavour=0 # default is light-jet
        if 4 in jets_flavour: WjetsFlavour=4 # 4 means c-jet
        if 5 in jets_flavour: WjetsFlavour=5 # 5 means b-jet
        #print "INFO: W+jets flavour classification is: ", WjetsFlavour


        # main cut that will stop if the W+jets selection is different from the one asked in the job option
        if Wjets_HF in [ "l", "c" , "b" ]:
            if Wjets_HF=="l" : Wjets_selection=0
            elif Wjets_HF=="c" : Wjets_selection=4
            elif Wjets_HF=="b" : Wjets_selection=5
            #print "INFO: W+jets Wjets_selection: ", Wjets_selection
            if WjetsFlavour != Wjets_selection : continue



    ###################################################################################################
    # MET
    MET = tree.met_met

    # mTW
    mTW_lep = rt.TMath.Sqrt( 2. * lep.Pt() * MET * ( 1. - rt.TMath.Cos( lep.Phi() - tree.met_phi ) ) )
    triangular = MET + mTW_lep
    #print "INFO INITIAL: lep_pt = %f and MET = %f"                % ( lep.Pt()/GeV, MET/GeV )


    # here I apply an initial MET > 30 and (MET+mTW) > 60 cuts
    if MET/GeV < 30 : continue 
    #if triangular/GeV < 60 : continue

    #if MET/GeV < 30 : continue
    if mTW_lep/GeV < 30 : continue



    ###################################################################################################
    # decide region
    region = ""

   # if jets_n == 2 : region += "2jetex"
   # if jets_n == 3 : region += "3jetex"
   # if jets_n == 4 : region += "4jetex"
   # if jets_n >= 5 : region += "5jetin"

    if jets_n >= 4 : region += "4jetin"

    if n_bjets == 0 : region += "0bex"
   # if n_bjets == 1 : region += "1bex"
   # if n_bjets == 2 : region += "2bex"
   # if n_bjets >= 3 : region += "3bin"

    if n_bjets >= 1 : region += "1bin"

    #print "INFO: region name is: ", region


    #print "MET = %f"                % ( MET/GeV )
    #print "mTW = %f"                % ( mTW_lep/GeV )


    ###################################################################################################
    # up to five highest pT jets, however can have suddenly events like 7j 4b, so at the end have j b b b b
    jets_five = []
    count = 1. # simple counter of overall jets
    countb =0. # and bjets
    for j in jets:
       if count < 6:
          jets_five += [ j ]
          count = count + 1
          if j.isBtagged : countb += 1 
    jets_five_n  = len( jets_five )
    #print "INFO: jets_five_n: ", jets_five_n

    n_bjets_five = countb
    light_jets = jets_n - n_bjets
    light_jets_five = jets_five_n - n_bjets_five # for five jets



    ###################################################################################################
    # scalar pT sum of all selected jets, HThad
    HT = 0.
    for j in jets:
        HT += j.Pt()
    #print "INFO: HT(jets): ", HT/GeV

 
    ###################################################################################################
    # variables
    dPhi_j1_lep = lep.DeltaPhi(jets[0])
    
    # _____________________________
    # EventShapeVariables
    FW2 = helper_functions.EventShapeVariables( [lep] + jets  )


    # _____________________________
    # CalcLeptonBJetVariables
    all_lb_dR = []
    median_dR_bl = 0.
    min_dR_bl = 0.

    all_lb_dEta = []
    median_dEta_bl = 0.
    min_dEta_bl = 0.


    if n_bjets >= 1 :
       all_lb_dR, all_lb_dEta = helper_functions.CalcLeptonBJetVariables( lep, bjets ) 
       median_dR_bl   = median(all_lb_dR)
       median_dEta_bl = median(all_lb_dEta)
       min_dEta_bl    = all_lb_dEta[0]
       min_dR_bl      = all_lb_dR[0]

    # _____________________________
    # CalcJetJetVariables
    all_jj_m = []
    all_jj_dR = []
    all_jj_dEta = []
    mjj_mindR = 0.
    dEta_jj_med = 0.         
    dR_jj_med = 0.
    dR_jj_min = 0.


    all_jj_m, all_jj_dR, all_jj_dEta, mjj_mindR = helper_functions.CalcJetJetVariables( jets )

    # medians
    dR_jj_med   = median(all_jj_dR) 
    dEta_jj_med = median(all_jj_dEta) 
    dR_jj_min   = all_jj_dR[0]

    # second smallest invariant mass between jet pairs.
    m_jj_second_min = 0.
    if jets_n > 2:
      m_jj_second_min = all_jj_m[1] / GeV


    # _____________________________
    # CalcUJetUJetVariables
    all_uu_m = []
    all_uu_dR = []
    all_uu_dEta = []
    dR_uu_med = 0.
    dR_uu_min = 0.
    muu_mindR = 0.

    if light_jets >= 2 :
       all_uu_m, all_uu_dR, all_uu_dEta, muu_mindR = helper_functions.CalcUJetUJetVariables( jets )
       dR_uu_med   = median(all_uu_dR) 
       dR_uu_min   = all_uu_dR[0]




    #number of jets with pT > 40 
    jets40_n = 0.
    for i in range(jets_n):
       pt  = jets[i].Pt()
       if pt/GeV > 40 : 
         jets40_n += 1




    ##################################### BEGIN SAVING HISTOGRAMS ##################################### 

    # begin saving plots
    option = 1
    if option == 1:
      for channel in [ "ljets", ch_label ]:

        # HT
        histograms[channel][region]['HT500'].Fill( HT/GeV, w )
        histograms[channel][region]['HT750'].Fill( HT/GeV, w )

        ### mva variables
        histograms[channel][region]['dR_jj_med'].Fill(dR_jj_med, w )
        histograms[channel][region]['dR_uu_med'].Fill(dR_uu_med, w )
        histograms[channel][region]['mjj_mindR'].Fill(mjj_mindR/GeV, w )
        histograms[channel][region]['muu_mindR'].Fill(muu_mindR/GeV, w )
        histograms[channel][region]['FW2'].Fill(FW2, w )
        histograms[channel][region]['median_dR_bl_bin4'].Fill(median_dR_bl, w)
        histograms[channel][region]['median_dR_bl_bin5'].Fill(median_dR_bl, w)

        ### kinematics
        histograms[channel][region]['lep_eta'].Fill( lep.Eta(), w )
        histograms[channel][region]['met'].Fill( MET/GeV, w )
        #if channel == "ejets" and region == "4jetin1bin":
            #print "INFO: Event " + str(ientry) + " marked as electron"
        histograms[channel][region]['mtw_lep'].Fill( mTW_lep/GeV, w )
        histograms[channel][region]['lep_pt'].Fill( lep.Pt()/GeV, w ) 
        histograms[channel][region]['lep_q'].Fill( lep.q, w ) 
        histograms[channel][region]['lep_phi'].Fill( lep.Phi(), w )

        histograms[channel][region]['j1_pt'].Fill( jets[0].Pt()/GeV, w )
        histograms[channel][region]['j1_eta'].Fill( jets[0].Eta(), w )
        histograms[channel][region]['j1_phi'].Fill( jets[0].Phi(), w )

        histograms[channel][region]['dPhi_j1_lep'].Fill( dPhi_j1_lep, w )

        if jets_n >= 2: 
            histograms[channel][region]['j2_pt'].Fill( jets[1].Pt()/GeV, w )
            histograms[channel][region]['j2_eta'].Fill( jets[1].Eta(), w )
            histograms[channel][region]['j2_phi'].Fill( jets[1].Phi(), w )

        if jets_n >= 3: 
            histograms[channel][region]['j3_eta'].Fill( jets[2].Eta(), w )
            histograms[channel][region]['j3_phi'].Fill( jets[2].Phi(), w )
            histograms[channel][region]['j3_pt'].Fill( jets[2].Pt()/GeV, w )

        if jets_n >= 4: 
            histograms[channel][region]['j4_eta'].Fill( jets[3].Eta(), w )
            histograms[channel][region]['j4_phi'].Fill( jets[3].Phi(), w )
            histograms[channel][region]['j4_pt'].Fill( jets[3].Pt()/GeV, w )



    # Njets inclusive must go at the end as region word is re-written
    for channel in [ "ljets", ch_label ]:
      if jets_n >= 4 and n_bjets >= 1 :
          region = "4jetin1bin"
          histograms[channel][region]['jets_N'].Fill( jets_n, w )
          histograms[channel][region]['bjets_N'].Fill( n_bjets, w)


# Write out
outfile.cd()
for channel in known_channels:
    for region in known_regions:
        # add overflow bin
        for h in histograms[channel][region].values() :
            if(h.GetBinContent(h.GetNbinsX()+1) > 0) :
                h.AddBinContent(h.GetNbinsX(), h.GetBinContent(h.GetNbinsX()+1))
        # save
        for h in histograms[channel][region].values(): h.Write()

#save file
outfile.Close()
print "INFO: output file created", outfilename
print "Sum of weights for file: ", sum_weights

