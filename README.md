ttbar-xsec-13p6TeV [![build status](https://gitlab.cern.ch/gguerrie/ttbar-xsec-13p6tev/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/gguerrie/ttbar-xsec-13p6tev/commits/master)
=================

**[Initial Setup](#initial-setup)** |
**[Structure](#structure)** |
**[Data preparation](#data-preparation)** |
**[Run the Analysis](#run-the-analysis)** 

# Disclaimer

This repository is inspired to [the 5TeV cross section measurement](https://gitlab.cern.ch/lserkin/ttbar_xs_5tev), so in order to have further insights, please feel free to explore it.

## Initial Setup

At the beginning of each session:
```
source env_setup.sh
```

## Structure

Both the single lepton and dilepton analyses are present in the repository, respectively belonging to the `SingleLeptonSelection` and `DileptonSelection` folders; the `auxiliary` folder hosts a set of additional tools useful to build up the file-lists, to produce the SumOfWeights file and to submit batch jobs with ht-condor.

## Data preparation
In order to perform the analysis, two different steps are necessary:
- Identify the list of `.root` files to be processed.
- Build the `.xml` file with the sum of weights and the systematic weights.

### Make the filelists
To obtain the filelist it is possible to run the `auxiliary/MakeFileList.sh` script:
```
source auxiliary/MakeFileList.sh -p <path_to_the_datasets_dir> -o <path_to_the_output_file>
```
where the directory indicated with the `-p` option must contain a set of folders of this type:
```
user.gguerrie.700341.Sh.DAOD_PHYS.e8351_s3681_r13145_p4926.R22-sinlgelep-wjets-v1.2-gguerrie_output_root/
user.gguerrie.700340.Sh.DAOD_PHYS.e8351_s3681_r13145_p4926.R22-sinlgelep-wjets-v1.2-gguerrie_output_root/
user.gguerrie.700339.Sh.DAOD_PHYS.e8351_s3681_r13145_p4926.R22-sinlgelep-wjets-v1.2-gguerrie_output_root/
user.gguerrie.700338.Sh.DAOD_PHYS.e8351_s3681_r13145_p4926.R22-sinlgelep-wjets-v1.2-gguerrie_output_root/
```
For each DSID, a textfile will be created. 

It is possible to merge the `.txt` files, grouping them up as follows
- ttbar
- diboson
- single Top (all DSIDs separated)
- W+jets (enu, munu, taunu separated)
- Z+jets (ee, emu, mumu separated)
by running:
```
source auxiliary/merge_filelist.sh -p <path_to_the_filelist_dir> -o <path_to_the_merged_filelist_dir>
```
### Creating the SumOfWeights file
To create the weight file execute
```
python auxiliary/CreateXml.py -i <path_to_the_filelist_dir> -o <path_to_the_output_file.xml>
```
**notice** that the option `-i` has to be set to the folder containing the DSIDs separated.

**IMPORTANT**: the path to the `.xml` has to be set at line 13 of `helpers_functions.py`, present in the according folder (`SingleLeptonSelection` or `DileptonSelection`)


## Run the Analysis

Create the `output` folder inside a specific selection directory:
```
mkdir output
```
Run selection (depends on the folder):
```
./SomeSelection.py -i <path_to_the_merged_filelist_dir>
```
The output of the script will by default appear in the `ouput/check` folder; below there is a list of the possible options for the script.
- `-i`, `--filelistname`; path to the file containing the list of samples (default="filelists/check.txt")
- `-o`, `--outfilename`; name of the output file  (default="")
- `-s`, `--systematic`; name of the systematic to be run  (default="nominal")
- `-p`, `--preselection`; name of the preseection, i.e. the folder that contains the output files (default="check")
- `-j`, `--jetcut`; cut on jet pt       (default="25")    
- `-HF`, `--Wjets_HF` (default="all")    

Git 
=================
```
setupATLAS
lsetup git
git clone https://USERNAME@gitlab.cern.ch/gguerrie/ttbar-xsec-13p6tev.git
```
